import 'package:flutter/material.dart';
import 'package:geminiapp/app/app.logger.dart';
import 'package:geminiapp/res/customcolors.dart';
import 'package:geminiapp/view/sizeconfig.dart';
import 'package:geminiapp/view_model/login_viewmodel.dart';
import 'package:stacked/stacked.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LoginPinView extends StatelessWidget {
  final log = getLogger('LoginPinView');
  LoginPinView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ViewModelBuilder<LoginViewModel>.reactive(
        viewModelBuilder: () => LoginViewModel(),
        onModelReady: (model) => {},
        builder: (context, model, child) {
          return Scaffold(
            backgroundColor: CustomColors.appBackground,
            body: Container(
              padding: EdgeInsets.symmetric(
                  vertical: SizeConfig.screenheight / 8,
                  horizontal: SizeConfig.screenwidth / 7),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    AppLocalizations.of(context)!.welcome,
                    style: const TextStyle(
                        fontWeight: FontWeight.w700,
                        color: CustomColors.colorPrimary,
                        fontSize: 20),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: SizeConfig.screenheight * 0.03,
                  ),
                  Text(
                    AppLocalizations.of(context)!.enterpin,
                    style: const TextStyle(fontSize: 15),
                  ),
                  SizedBox(
                    height: SizeConfig.screenheight * 0.03,
                  ),
                  TextField(
                    controller: model.pinController,
                    decoration: InputDecoration(
                        icon: const Icon(
                          Icons.lock,
                          color: Colors.blue,
                        ),
                        border: const OutlineInputBorder(),
                        hintText: AppLocalizations.of(context)!.pinhint),
                  ),
                  SizedBox(
                    height: SizeConfig.screenheight * 0.02,
                  ),
                  ElevatedButton(
                      onPressed: () {
                        model.authentication(model.pinController.text);
                      },
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: SizeConfig.screenwidth / 25,
                            vertical: SizeConfig.screenheight / 75),
                        child: Text(
                          AppLocalizations.of(context)!.continuet,
                          style: const TextStyle(fontSize: 18),
                        ),
                      ))
                ],
              ),
            ),
          );
        });
  }
}
