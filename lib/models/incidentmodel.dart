import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'eventsmodel.dart';
import 'linksmodel.dart';
import 'locationmodel.dart';

enum Source { UNKNOWN, DRIVER, RIDER, TAG, EXT_BUTTON, HIGH_ACC }

class IncidentModel {
  String? id;
  String? createdDate;
  int? triggerDate;
  Location? location;
  String? tagMacAddress;
  Links? links;
  List<Event>? events;
  String? city;
  String? state;
  Source? source;
  bool? isViewed;
  String? trigger;
  int? mediaStartDatetime;
  int? mediaEndDatetime;
  bool? complete;
  int? userId;
  String? deviceId;
  String? country;
  String? ambaStatusAtTriggerTime;
  String? tripStatusAtTriggerTime;
  String? driveState;

  IncidentModel({
    this.id,
    this.createdDate,
    this.triggerDate,
    this.location,
    this.tagMacAddress,
    this.links,
    this.events,
    this.city,
    this.state,
    this.source,
    this.isViewed,
    this.trigger,
    this.mediaStartDatetime,
    this.mediaEndDatetime,
    this.complete,
    this.userId,
    this.deviceId,
    this.country,
    this.ambaStatusAtTriggerTime,
    this.tripStatusAtTriggerTime,
    this.driveState,
  });

  IncidentModel copyWith({
    String? id,
    String? createdDate,
    int? triggerDate,
    Location? location,
    String? tagMacAddress,
    Links? links,
    List<Event>? events,
    String? city,
    String? state,
    Source? source,
    bool? isViewed,
    String? trigger,
    int? mediaStartDatetime,
    int? mediaEndDatetime,
    bool? complete,
    int? userId,
    String? deviceId,
    String? country,
    String? ambaStatusAtTriggerTime,
    String? tripStatusAtTriggerTime,
    String? driveState,
  }) {
    return IncidentModel(
      id: id ?? this.id,
      createdDate: createdDate ?? this.createdDate,
      triggerDate: triggerDate ?? this.triggerDate,
      location: location ?? this.location,
      tagMacAddress: tagMacAddress ?? this.tagMacAddress,
      links: links ?? this.links,
      events: events ?? this.events,
      city: city ?? this.city,
      state: state ?? this.state,
      source: source ?? this.source,
      isViewed: isViewed ?? this.isViewed,
      trigger: trigger ?? this.trigger,
      mediaStartDatetime: mediaStartDatetime ?? this.mediaStartDatetime,
      mediaEndDatetime: mediaEndDatetime ?? this.mediaEndDatetime,
      complete: complete ?? this.complete,
      userId: userId ?? this.userId,
      deviceId: deviceId ?? this.deviceId,
      country: country ?? this.country,
      ambaStatusAtTriggerTime:
          ambaStatusAtTriggerTime ?? this.ambaStatusAtTriggerTime,
      tripStatusAtTriggerTime:
          tripStatusAtTriggerTime ?? this.tripStatusAtTriggerTime,
      driveState: driveState ?? this.driveState,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'date': createdDate,
      'trigger_date': triggerDate,
      'location': location?.toMap(),
      'tag_mac_address': tagMacAddress,
      '_links': links?.toMap(),
      'events': events?.map((x) => x.toMap()).toList(),
      'city': city,
      'state': state,
      'source': source?.index,
      'isViewed': isViewed,
      'trigger': trigger,
      'media_start_datetime': mediaStartDatetime,
      'media_end_datetime': mediaEndDatetime,
      'complete': complete,
      'user_id': userId,
      'device_id': deviceId,
      'country': country,
      'amba_status_at_trigger_time': ambaStatusAtTriggerTime,
      'trip_status_at_trigger_time': tripStatusAtTriggerTime,
      'drive_state': driveState,
    };
  }

  factory IncidentModel.fromMap(Map<String, dynamic> map) {
    int val1 = 0;
    if (map.containsKey('source')) {
      for (var element in Source.values) {
        if (element.name == map['source']) val1 = element.index;
      }
    }
    return IncidentModel(
      id: map['id'],
      createdDate: map['date'],
      triggerDate: map['trigger_date']?.toInt(),
      location:
          map['location'] != null ? Location.fromMap(map['location']) : null,
      tagMacAddress: map['tag_mac_address'],
      links: map['_links'] != null ? Links.fromMap(map['_links']) : null,
      events: map['events'] != null
          ? List<Event>.from(map['events']?.map((x) => Event.fromMap(x)))
          : null,
      city: map['city'],
      state: map['state'],
      source: map['source'] != null ? Source.values[val1] : null,
      isViewed: map['isViewed'],
      trigger: map['trigger'],
      mediaStartDatetime: map['media_start_datetime']?.toInt(),
      mediaEndDatetime: map['media_end_datetime']?.toInt(),
      complete: map['complete'],
      userId: map['user_id']?.toInt(),
      deviceId: map['device_id'],
      country: map['country'],
      ambaStatusAtTriggerTime: map['amba_status_at_trigger_time'],
      tripStatusAtTriggerTime: map['trip_status_at_trigger_time'],
      driveState: map['drive_state'],
    );
  }

  String toJson() => json.encode(toMap());

  factory IncidentModel.fromJson(String source) =>
      IncidentModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'IncidentModel(id: $id, createdDate: $createdDate, triggerDate: $triggerDate, location: $location, tagMacAddress: $tagMacAddress, links: $links, events: $events, city: $city, state: $state, source: $source, isViewed: $isViewed, trigger: $trigger, mediaStartDatetime: $mediaStartDatetime, mediaEndDatetime: $mediaEndDatetime, complete: $complete, userId: $userId, deviceId: $deviceId, country: $country, ambaStatusAtTriggerTime: $ambaStatusAtTriggerTime, tripStatusAtTriggerTime: $tripStatusAtTriggerTime, driveState: $driveState)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is IncidentModel &&
        other.id == id &&
        other.createdDate == createdDate &&
        other.triggerDate == triggerDate &&
        other.location == location &&
        other.tagMacAddress == tagMacAddress &&
        other.links == links &&
        listEquals(other.events, events) &&
        other.city == city &&
        other.state == state &&
        other.source == source &&
        other.isViewed == isViewed &&
        other.trigger == trigger &&
        other.mediaStartDatetime == mediaStartDatetime &&
        other.mediaEndDatetime == mediaEndDatetime &&
        other.complete == complete &&
        other.userId == userId &&
        other.deviceId == deviceId &&
        other.country == country &&
        other.ambaStatusAtTriggerTime == ambaStatusAtTriggerTime &&
        other.tripStatusAtTriggerTime == tripStatusAtTriggerTime &&
        other.driveState == driveState;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        createdDate.hashCode ^
        triggerDate.hashCode ^
        location.hashCode ^
        tagMacAddress.hashCode ^
        links.hashCode ^
        events.hashCode ^
        city.hashCode ^
        state.hashCode ^
        source.hashCode ^
        isViewed.hashCode ^
        trigger.hashCode ^
        mediaStartDatetime.hashCode ^
        mediaEndDatetime.hashCode ^
        complete.hashCode ^
        userId.hashCode ^
        deviceId.hashCode ^
        country.hashCode ^
        ambaStatusAtTriggerTime.hashCode ^
        tripStatusAtTriggerTime.hashCode ^
        driveState.hashCode;
  }
}
