import 'package:flutter/material.dart';
import 'package:geminiapp/res/customcolors.dart';

class CustomGradient {
  static LinearGradient get linearGradient {
    //1
    return const LinearGradient(
      begin: Alignment.topLeft,
      end: Alignment.centerLeft,
      colors: [CustomColors.colorPrimaryDark, CustomColors.colorPrimary],
    );
  }
}

class CustomDecoration {
  static BoxDecoration get decoration {
    return const BoxDecoration(
      color: CustomColors.colorBackground,
      borderRadius: BorderRadius.only(
        topLeft: Radius.circular(20),
        topRight: Radius.circular(20),
      ),
    );
  }
}

class HeaderPadding {
  static Padding get headPadding {
    return const Padding(
      padding: EdgeInsets.all(50),
      child: Text(
        'All Videos',
        style: TextStyle(
          decoration: TextDecoration.none,
          fontSize: 25,
          fontWeight: FontWeight.bold,
          color: Colors.white,
        ),
      ),
    );
  }
}



