import 'package:geminiapp/app/app.locator.dart';
import 'package:geminiapp/app/app.logger.dart';
import 'package:geminiapp/services/sdkservices.dart';
import 'package:geminiapp/services/vehiclesservices.dart';
import 'package:geminiapp/view/camerasettings_view.dart';
import 'package:stacked/stacked.dart';

class CamerasettingsViewModel extends BaseViewModel {
  final log = getLogger('CamerasettingsViewModel');
  bool is_valid_vehicle_id=true;
  final VehiclesModelServices _vehiclesModelServices =
      locator<VehiclesModelServices>();
  final _sdkService = locator<SDKServices>();

  String OnorOff(bool val){
    if(val==true){
      return "ON";
    }
    else{
      return "OFF";
    }
  }

  Future updateSettings(String input) async {
    var shortVehicleid = "";
    var outwardCamera = OnorOff(Camerasettings_View.isSwitched_road);
    var inwardCamera = OnorOff(Camerasettings_View.isSwitched_cabin);

    try {
      if (_vehiclesModelServices.VehicleResponse.vehicles.isNotEmpty) {
        shortVehicleid = _vehiclesModelServices
            .VehicleResponse.vehicles[0].short_vehicle_id
            .toString();
        var jsonData =
            "{\"short_vehicle_id\":\"$shortVehicleid\",\"outward_camera_setting\":\"$outwardCamera\",\"inward_camera_setting\":\"$inwardCamera\"}";
        setBusy(true);
        if(shortVehicleid !="0"){
          var result = await _sdkService.sdkUpdateCameraSettings(jsonData);
          log.d(" Camera Settings $result");
          notifyListeners();
        }
        else{
          is_valid_vehicle_id=false;
          log.d("Invalid short_vehicle_id");
        }

      }
    } catch (e) {
      log.d("Error Message :$e");
      return "error Message" + e.toString();
    }
  }
}
