
import 'package:flutter/cupertino.dart';
import 'package:injectable/injectable.dart';

import '../app/app.locator.dart';
import '../app/app.logger.dart';
import '../models/cubaloginresponse.dart';
import 'api.dart';
import 'sdkservices.dart';

@lazySingleton
class LoginModelServices{
  final log = getLogger('LoginModelServices');

  late bool _isAuthenticated = false;
  bool get isAuthenticated => _isAuthenticated;

  late CubaLoginResponse _loginResponse;
  CubaLoginResponse get loginResponse => _loginResponse;
  final _apiService = locator<Api>();
  final _sdkService = locator<SDKServices>();


  Future loginWithCode({
  @required authorizationCode
  }) async{
    try{
      _loginResponse =  await _apiService.getAccountDetails(authorizationCode);
      return _loginResponse;
    }catch(e){
      return "error Message";
    }
  }

  Future sdkAuthentication({@required loginDetails}) async{
    try{
      _isAuthenticated =  await _sdkService.sdkAuthentication(loginDetails);
      return _isAuthenticated;

    }catch(e){
      return "error Message $e";
    }
  }
}