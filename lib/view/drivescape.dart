import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:geminiapp/app/app.router.dart';
import 'package:geminiapp/res/customcolors.dart';
import 'package:geminiapp/res/themedata.dart';
import 'package:geminiapp/view/dashboard_view.dart';
import 'package:geminiapp/view/login_view.dart';
import 'package:geminiapp/view/welcome_drivescape_view.dart';
import 'package:stacked_services/stacked_services.dart';

class DriveScape extends StatelessWidget {
  const DriveScape({Key? key}) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Drivescape APP',
      localizationsDelegates: const [
        AppLocalizations.delegate, // Add this line
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('en', ''), // English, no country code
        Locale('es', ''),
      ],
      theme: AppTheme.lightTheme,
      navigatorKey: StackedService.navigatorKey,
      home: const Scaffold(
        backgroundColor: CustomColors.appBackground,
        body: WelcomeView(),
      ),
      onGenerateRoute: StackedRouter().onGenerateRoute,
    );
  }
}
