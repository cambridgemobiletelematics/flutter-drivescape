import 'package:geminiapp/services/incidentservices.dart';
import 'package:geminiapp/services/sdkservices.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:url_launcher/url_launcher.dart';

import '../app/app.locator.dart';
import '../app/app.router.dart';

class WelcomeViewModel extends BaseViewModel {
  String _declare = 'yet to build the screen based on gemini app';
  String get myDeclare => _declare;
  final _navigationService = locator<NavigationService>();
  final _sdkSevices = locator<SDKServices>();
  final IncidentListServices _incidentListServices =
      locator<IncidentListServices>();

  void updateDeclaration() {
    _declare = 'yet to build';
    notifyListeners();
  }

  void navigateToUberLoginView() {
    _navigationService.navigateTo(Routes.uberLoginView);
  }

  void navigateToLoginView() {
    _navigationService.navigateTo(Routes.loginView);
  }

  void initateCMTSDK() {
    // if (_incidentListServices.IncidentListResponse.incidentList.isNotEmpty) {
    //   navigateToHomeView();
    // } else {
    _sdkSevices.cmtsdkInitialization();
    //}
  }

  void launchurl_terms() async {
    String _url =
        "https://drivescape.cmtelematics.com/en/uber/terms-and-conditions/";
    if (!await launch(_url)) throw 'Could not launch $_url';
  }

  void launchurl_privacypolicy() async {
    String _url = "https://drivescape.cmtelematics.com/en/uber/privacy-policy/";
    if (!await launch(_url)) throw 'Could not launch $_url';
  }
}
