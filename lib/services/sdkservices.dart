import 'package:injectable/injectable.dart';
import 'package:flutter_geminisdk/flutter_geminisdk.dart';

import '../app/app.logger.dart';

@lazySingleton
class SDKServices {
  final log = getLogger('SDKServices');
  Future<dynamic> cmtsdkInitialization() async {
    final status = await FlutterGeminisdk.cmtsdkInitialization();
    return status;
  }

  Future<dynamic> sdkAuthentication(String loginDetails) async {
    final result = await FlutterGeminisdk.sdkAuthentication(loginDetails);
    return result;
  }

  Future<dynamic> sdkGetVehicles() async {
    final status = await FlutterGeminisdk.getVehicles();

    return status;
  }

  Future<dynamic> sdkGetIncidentList(String url) async {
    final status = await FlutterGeminisdk.getIncidentList(url);
    return status;
  }

  Future<dynamic> sdkGetMediaData(String url) async {
    final status = await FlutterGeminisdk.getMediaDataFlow(url);
    return status;
  }

  Future<dynamic> sdkGetDeviceParams() async {
    final status = await FlutterGeminisdk.getDeviceParams();
    return status;
  }

  Future<dynamic> sdkUpdateCameraSettings(String jsonReq) async {
    final status = await FlutterGeminisdk.updateCameraSettings(jsonReq);
    return status;
  }
}
