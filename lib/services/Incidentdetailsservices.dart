import 'package:geminiapp/app/app.locator.dart';
import 'package:geminiapp/app/app.logger.dart';
import 'package:geminiapp/models/deviceparams.dart';
import 'package:geminiapp/services/sdkservices.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class IncidentDetails {
  final log = getLogger('IncidentDetailsServices');

  late DeviceParams _deviceParams;
  DeviceParams get deviceParams => _deviceParams;
  final _sdkService = locator<SDKServices>();

  Future getDeviceParams() async {
    try {
      var response = await _sdkService.sdkGetDeviceParams();
      _deviceParams = DeviceParams.fromJson(response);
      return _deviceParams;
    } catch (e) {
      return "error Message $e";
    }
  }
}
