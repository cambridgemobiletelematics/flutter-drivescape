import 'package:geminiapp/app/app.locator.dart';
import 'package:geminiapp/app/app.logger.dart';
import 'package:geminiapp/app/app.router.dart';
import 'package:geminiapp/services/Incidentdetailsservices.dart';
import 'package:geminiapp/view/allvideos_view.dart';
import 'package:geminiapp/view/needhelp_ledlights_view.dart';
import 'package:geminiapp/models/cubaloginresponse.dart';
import 'package:geminiapp/models/helpmodel.dart';
import 'package:geminiapp/models/incidentmodel.dart';
import 'package:geminiapp/services/helpservices.dart';
import 'package:geminiapp/services/incidentservices.dart';
import 'package:geminiapp/services/loginmodel.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class DashboardViewModel extends BaseViewModel {
  final log = getLogger('DashboardViewModel');
  final LoginModelServices _loginServices = locator<LoginModelServices>();
  final IncidentListServices _incidentListServices =
      locator<IncidentListServices>();
  late List<IncidentModel> _incidentList;
  List<IncidentModel> get incidentList => _incidentList;
  final _navigationService = locator<NavigationService>();
  final _helper = locator<HelpModelServices>();
  late List<HelpModel> _HelpList;
  List<HelpModel> get HelperList => _HelpList;
  final _deviceParams = locator<IncidentDetails>();

  void onLoad() {
    // CubaLoginResponse login = _loginServices.loginResponse;
    // if (login.accountId.isNotEmpty) {
    //   _incidentList = _incidentListServices.IncidentListResponse.incidentList;
    //   _HelpList = _helper.HelpService;
    //   notifyListeners();
    // }

    if (_incidentListServices.IncidentListResponse.incidentList.isNotEmpty) {
      _incidentList = _incidentListServices.IncidentListResponse.incidentList;
      _HelpList = _helper.HelpService;
      notifyListeners();
    }
    _deviceParams.getDeviceParams();
  }

  String getUserInitiatedImage(int index) {
    if (_incidentList.isNotEmpty) {
      if (_incidentList[index].source == Source.EXT_BUTTON) {
        return 'assets/images/UserInitiated.png';
      } else if (_incidentList[index].source == Source.HIGH_ACC) {
        return 'assets/images/HardBraking.png';
      }
    }
    return 'assets/images/UserInitiated.png';
  }

  String getLocation(int index) {
    if (_incidentList.isNotEmpty) {
      if (_incidentList[index].city != null ||
          _incidentList[index].state != null) {
        return 'Location ' +
            _incidentList[index].city! +
            ' ' +
            _incidentList[index].state!;
      } else {
        return "Location UNKNOWN";
      }
    }
    return 'Location';
  }

  String getTrigger(int index) {
    if (_incidentList.isNotEmpty) {
      if (_incidentList[index].source == Source.EXT_BUTTON) {
        return 'Button Press';
      } else if (_incidentList[index].source == Source.HIGH_ACC) {
        return 'Hard brake';
      } else if (_incidentList[index].source != null) {
        return _incidentList[index].source.toString();
      }
    }
    print(_incidentList[index].source.toString());
    return _incidentList[index].source.toString();
  }

  void navigateToAllVideos() {
    _navigationService.navigateTo(Routes.allVideosView);
  }

  void navigateToIncidentDetails(int index) {
    _navigationService.navigateTo(Routes.incidentDetailsView,
        arguments: IncidentDetailsViewArguments(indexValue: index));
  }

  void navigateToFindout(int index) {
    switch (index) {
      case 0:
        _navigationService.navigateTo(Routes.needhelp_Ledlights_View);
        break;
      case 1:
        _navigationService.navigateTo(Routes.feedback_View);
        break;
      case 2:
        _navigationService.navigateTo(Routes.contactsupport_View);
        break;
    }
  }
}
