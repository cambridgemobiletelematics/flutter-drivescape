import 'package:geminiapp/app/app.router.dart';
import 'package:geminiapp/models/incidentmodel.dart';
import 'package:geminiapp/services/incidentservices.dart';
import 'package:geminiapp/services/loginmodel.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import '../app/app.locator.dart';
import '../app/app.logger.dart';

class AllVideosViewModel extends BaseViewModel {
  final log = getLogger('AllVideosViewModel');
  final LoginModelServices _loginServices = locator<LoginModelServices>();
  final IncidentListServices _incidentListServices =
      locator<IncidentListServices>();
  late List<IncidentModel> _incidentList;
  List<IncidentModel> get incidentList => _incidentList;
  final _navigationService = locator<NavigationService>();

  // final _deviceParams = locator<IncidentDetails>();

  void getaccount() {
    getIncidentList();
  }

  void getIncidentList() {
    _incidentList = _incidentListServices.IncidentListResponse.incidentList;
  }

  String getUserInitiatedImage(int index) {
    if (_incidentList.isNotEmpty) {
      if (_incidentList[index].source == Source.EXT_BUTTON) {
        return 'assets/images/UserInitiated.png';
      } else if (_incidentList[index].source == Source.HIGH_ACC) {
        return 'assets/images/HardBraking.png';
      }
    }
    return '';
  }

  String getLocation(int index) {
    if (_incidentList.isNotEmpty) {
      if (_incidentList[index].city != null ||
          _incidentList[index].state != null) {
        return 'Location ' +
            _incidentList[index].city! +
            ' ' +
            _incidentList[index].state!;
      } else {
        return "Location UNKNOWN";
      }
    }
    return 'Location';
  }

  String getTrigger(int index) {
    if (_incidentList.isNotEmpty) {
      if (_incidentList[index].source == Source.EXT_BUTTON) {
        return 'Button Press';
      } else if (_incidentList[index].source == Source.HIGH_ACC) {
        return 'Hard brake';
      } else if (_incidentList[index].source != null) {
        return _incidentList[index].source.toString();
      }
    }
    return 'UNKNOWN';
  }

  void navigateToIncidentDetails(int index) {
    _navigationService.navigateTo(Routes.incidentDetailsView,
        arguments: IncidentDetailsViewArguments(indexValue: index));
  }
}
