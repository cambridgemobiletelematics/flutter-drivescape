import 'package:flutter/material.dart';

import 'customcolors.dart';

class AppTheme {
  static ThemeData get lightTheme {
    //1
    return ThemeData(
        //2
        colorScheme:
            ColorScheme.fromSwatch(primarySwatch: Colors.blue).copyWith(
          secondary: Colors.green,
        ),
        primaryColor: const Color.fromRGBO(254, 248, 248, 1),
        scaffoldBackgroundColor: Colors.white,
        fontFamily: 'Montserrat', //3
        buttonTheme: ButtonThemeData(
          // 4
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(18.0)),
          buttonColor: CustomColors.lightPurple,
        ));
  }
}
