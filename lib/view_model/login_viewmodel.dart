import 'package:flutter/material.dart';
import 'package:geminiapp/app/app.locator.dart';
import 'package:geminiapp/app/app.logger.dart';
import 'package:geminiapp/app/app.router.dart';
import 'package:geminiapp/models/incidentlistmodel.dart';
import 'package:geminiapp/models/vehiclesmodel.dart';
import 'package:geminiapp/res/constants.dart';
import 'package:geminiapp/services/helpservices.dart';
import 'package:geminiapp/services/incidentservices.dart';
import 'package:geminiapp/services/loginmodel.dart';
import 'package:geminiapp/services/vehiclesservices.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class LoginViewModel extends BaseViewModel {
  final log = getLogger('LoginViewModel');
  final _navigationService = locator<NavigationService>();
  final LoginModelServices _loginServices = locator<LoginModelServices>();
  final _dialogService = locator<DialogService>();
  final IncidentListServices _incidentListServices =
      locator<IncidentListServices>();
  final _helper = locator<HelpModelServices>();

  static String emailId = "";
  TextEditingController emailController = TextEditingController();
  TextEditingController pinController = TextEditingController();

  final VehiclesModelServices _vehiclesModelServices =
      locator<VehiclesModelServices>();

  void navigateToPinView() {
    _navigationService.navigateTo(Routes.loginPinView);
  }

  void navigateToHomeView() {
    _navigationService.navigateTo(Routes.homeView);
  }

  void navigateToLoginView() {
    _navigationService.navigateTo(Routes.loginView);
  }

  Future authentication(String data) async {
    String authCode = "";
    if (emailId.isEmpty) {
      emailId = data;
    } else {
      authCode = data;
    }
    var jsonData =
        "{\"emailId\":\"$emailId\",\"authCode\":\"$authCode\",\"accountId\":\"\"}";
    setBusy(true);
    var result = await _loginServices.sdkAuthentication(loginDetails: jsonData);
    notifyListeners();
    if (result is bool) {
      if (result) {
        if (authCode.isNotEmpty && emailId.isNotEmpty) {
          log.d(" login successful");
          var results = await _vehiclesModelServices.getVehicles();
          if (results is VehiclesModels) {
            log.d(" got vehicles $results");
            log.d(_vehiclesModelServices.VehicleResponse.vehicles.length);
            if (_vehiclesModelServices.VehicleResponse.vehicles.isNotEmpty) {
              var results = await _incidentListServices.getIncidentList();
              var re = await _helper.setHelperList();
              notifyListeners();
              if (results is IncidentListModel) {
                log.d(" received incident list");
                log.d(_incidentListServices
                    .IncidentListResponse.incidentList.length);
                navigateToHomeView();
              } else {
                log.d(" failed to get incident list +${results}");
                //show dialog
                await _dialogService.showCustomDialog(
                  variant: DialogType.basic,
                  title: ' VEHICLES ',
                  description: ' EMPTY Vehicles',
                  mainButtonTitle: 'ok',
                );
                emailId = "";
                authCode = "";
                emailController.text = "";
                navigateToLoginView();
              }
            }else{
              await _dialogService.showCustomDialog(
                variant: DialogType.basic,
                title: ' VEHICLES ',
                description: ' EMPTY Vehicles',
                mainButtonTitle: 'ok',
              );
              emailId = "";
              authCode = "";
              emailController.text = "";
              navigateToLoginView();
            }
          }else{
            await _dialogService.showCustomDialog(
              variant: DialogType.basic,
              title: ' VEHICLES ',
              description: ' Vehicles{$results}',
              mainButtonTitle: 'ok',
            );
            emailId = "";
            authCode = "";
            emailController.text = "";
            navigateToLoginView();
          }
        } else {
          navigateToPinView();
        }
      } else {
        await _dialogService.showCustomDialog(
          variant: DialogType.basic,
          title: ' Login ',
          description: ' Not a valid email id or pass code',
          mainButtonTitle: 'ok',
        );
        emailId = "";
        authCode = "";
        emailController.text = "";
        notifyListeners();
      }
    }
  }
}
