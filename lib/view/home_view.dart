import 'package:flutter/material.dart';
import 'package:geminiapp/view/allvideos_view.dart';
import 'package:geminiapp/view/dashboard_view.dart';
import 'package:geminiapp/view/help_view.dart';
import 'package:geminiapp/view_model/home_viewmodel.dart';
import 'package:stacked/stacked.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeViewModel>.reactive(
        viewModelBuilder: () => HomeViewModel(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: getViewForIndex(viewModel.currentIndex),
            bottomNavigationBar: BottomNavigationBar(
                type: BottomNavigationBarType.fixed,
                backgroundColor: const Color.fromARGB(255, 243, 239, 239),
                currentIndex: viewModel.currentIndex,
                onTap: viewModel.setIndex,
                items: const [
                  BottomNavigationBarItem(
                    label: 'Home',
                    icon: Icon(Icons.home),
                  ),
                  BottomNavigationBarItem(
                    label: 'Videos',
                    icon: Icon(Icons.play_arrow_outlined),
                  ),
                  BottomNavigationBarItem(
                    label: 'Help',
                    icon: Icon(Icons.help_outlined),
                  )
                ]),
          );
        });
  }

  Widget getViewForIndex(int index) {
    switch (index) {
      case 0:
        return const DashboardView();
      case 1:
        return const AllVideosView();
      default:
        return const Help_View();
    }
  }
}
