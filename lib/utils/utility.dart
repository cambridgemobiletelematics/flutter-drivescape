import 'package:intl/intl.dart';

class Utility {
  static String getDate(String date) {
    if (date.isNotEmpty) {
      DateFormat df = DateFormat('EEE, MMM d, ' 'yyyy');
      DateTime dt = DateTime.parse(date);
      return df.format(dt);
    } else {
      return "";
    }
  }

  static String getTime(String date) {
    if (date.isNotEmpty) {
      DateFormat df = DateFormat('h:mm a');
      DateTime dt = DateTime.parse(date);
      return df.format(dt);
    } else {
      return "";
    }
  }

  static String getFormatedDate(DateTime? date) {
    if (date != null) {
      return DateFormat('EEE, MMM d, ' 'yyyy').format(date);
    } else {
      return " ";
    }
  }
}
