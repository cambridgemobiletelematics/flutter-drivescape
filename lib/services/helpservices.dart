import 'package:geminiapp/app/app.logger.dart';
import 'package:geminiapp/models/helpmodel.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class HelpModelServices {
  final log = getLogger('HelpModelServices');

  late List<HelpModel> _helpListModel;

  List<HelpModel> get HelpService => _helpListModel;

  var lightData = HelpModel();
  var feedbackData = HelpModel();
  var contactData = HelpModel();

  void setModelValue() {
    lightData.title = "LED lights";
    lightData.subTitle = "Want to know what those\nLED lights mean?";
    lightData.buttonText = "Find out";
    lightData.icon = "assets/images/SupportCards.png";

    feedbackData.title = "Provide feedback";
    feedbackData.subTitle = "Do you have any feedback \n we should know about?";
    feedbackData.buttonText = "Let us know";
    feedbackData.icon = "assets/images/Feedback.png";

    contactData.title = "Having issue?";
    contactData.subTitle = "Our support team is here to help!";
    contactData.buttonText = "Contact support";
    contactData.icon = "assets/images/Headphone.png";
  }

  List<HelpModel> setHelperList() {
    List<HelpModel> helper = [];
    setModelValue();
    helper.add(lightData);
    helper.add(feedbackData);
    helper.add(contactData);
    _helpListModel = helper;
    return helper;
  }
}
