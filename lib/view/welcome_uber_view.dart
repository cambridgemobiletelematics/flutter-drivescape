import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:geminiapp/view/sizeconfig.dart';
import 'package:geminiapp/view_model/welcome_viewmodel.dart';
import 'package:stacked/stacked.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import '../res/commonwidget.dart';

class WelcomeUberView extends StatelessWidget {
  const WelcomeUberView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    Size size = MediaQuery.of(context).size;
    return ViewModelBuilder<WelcomeViewModel>.reactive(
      viewModelBuilder: () => WelcomeViewModel(),
      onModelReady: (model) => model.initateCMTSDK(),
      builder: (context, model, child) {
        return Container(
            decoration: buildWelcomeBGBoxDecoration(),
            child: Scaffold(
              backgroundColor: Colors.transparent,
              body: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Center(
                      child: buildWelcomeLogoContainer(size)),
                  Center(
                    child: buildWelcomeTextPadding(size),
                  ),
                  SizedBox(
                    width: size.width,
                    height: size.height / 2.35,
                  ),
                  Center(
                      child: Padding(
                          padding: EdgeInsets.all(0),
                          child: ElevatedButton(
                            onPressed: () => model.navigateToUberLoginView(),
                            style: ElevatedButton.styleFrom(
                              primary: Color.fromRGBO(28, 119, 195, 1),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0),
                              ),
                            ),
                            child: Padding(
                              padding: EdgeInsets.only(
                                  top: size.height/36,
                                  left: size.width / 4,
                                  bottom: size.height/36,
                                  right: size.width / 4),
                              child: Text(
                                AppLocalizations.of(context)!.getStarted,
                                style: TextStyle(
                                    fontSize: size.height/42, color: Colors.white),
                              ),
                            ),
                          ))),
                  Center(
                      child: Container(
                          padding:
                              EdgeInsets.only(top: size.height / 32, bottom: 0),
                          child: Text("By signing up you accept our ",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize:size.height/56,
                                color: Colors.white,
                              )))),
                  Center(
                      child: RichText(
                          text: TextSpan(
                    children: [
                      TextSpan(
                        text: "Terms & Condition ",
                        style:TextStyle(
                          fontSize: size.height/56,
                          color: Colors.white,
                        ),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () => model.launchurl_terms(),
                      ),
                      TextSpan(
                          text: "and",
                          style: TextStyle(
                            fontSize: size.height/56,
                            color: Colors.white,
                          )),
                      TextSpan(
                          text: " Privacy Policy ",
                          style: TextStyle(
                            fontSize: size.height/56,
                            color: Colors.white,
                          ),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () => model.launchurl_privacypolicy()),
                    ],
                  ))),
                  Center(
                      child: buildCMTLogoContainer(size)),
                ],
              ),
            ));
      },
    );
  }


}
