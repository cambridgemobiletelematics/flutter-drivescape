// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// StackedLocatorGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:stacked/stacked.dart';
import 'package:stacked/stacked_annotations.dart';
import 'package:stacked_services/stacked_services.dart';

import '../services/Incidentdetailsservices.dart';
import '../services/api.dart';
import '../services/helpservices.dart';
import '../services/incidentservices.dart';
import '../services/loginmodel.dart';
import '../services/mediadataservices.dart';
import '../services/sdkservices.dart';
import '../services/vehiclesservices.dart';

final locator = StackedLocator.instance;

void setupLocator({String? environment, EnvironmentFilter? environmentFilter}) {
// Register environments
  locator.registerEnvironment(
      environment: environment, environmentFilter: environmentFilter);

// Register dependencies
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => Api());
  locator.registerLazySingleton(() => LoginModelServices());
  locator.registerLazySingleton(() => VehiclesModelServices());
  locator.registerLazySingleton(() => SDKServices());
  locator.registerLazySingleton(() => IncidentListServices());
  locator.registerLazySingleton(() => MediaDataServices());
  locator.registerLazySingleton(() => HelpModelServices());
  locator.registerLazySingleton(() => IncidentDetails());
  locator.registerLazySingleton(() => DialogService());
  locator.registerLazySingleton(() => SnackbarService());
}
