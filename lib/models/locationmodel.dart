import 'dart:convert';

class Location {
  double? sp;
  int? ts;
  double? acc;
  double? lat;
  double? lon;
  String? address;
  Location({
    this.sp,
    this.ts,
    this.acc,
    this.lat,
    this.lon,
    this.address,
  });

  Location copyWith({
    double? sp,
    int? ts,
    double? acc,
    double? lat,
    double? lon,
    String? address,
  }) {
    return Location(
      sp: sp ?? this.sp,
      ts: ts ?? this.ts,
      acc: acc ?? this.acc,
      lat: lat ?? this.lat,
      lon: lon ?? this.lon,
      address: address ?? this.address,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'sp': sp,
      'ts': ts,
      'acc': acc,
      'lat': lat,
      'lon': lon,
      'address': address,
    };
  }

  factory Location.fromMap(Map<String, dynamic> map) {
    return Location(
      sp: map['sp']?.toDouble(),
      ts: map['ts']?.toInt(),
      acc: map['acc']?.toDouble(),
      lat: map['lat']?.toDouble(),
      lon: map['lon']?.toDouble(),
      address: map['address'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Location.fromJson(String source) => Location.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Location(sp: $sp, ts: $ts, acc: $acc, lat: $lat, lon: $lon, address: $address)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is Location &&
      other.sp == sp &&
      other.ts == ts &&
      other.acc == acc &&
      other.lat == lat &&
      other.lon == lon &&
      other.address == address;
  }

  @override
  int get hashCode {
    return sp.hashCode ^
      ts.hashCode ^
      acc.hashCode ^
      lat.hashCode ^
      lon.hashCode ^
      address.hashCode;
  }
}
