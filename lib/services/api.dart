import 'dart:convert';


import 'package:geminiapp/models/cubaloginresponse.dart';
import 'package:injectable/injectable.dart';
import 'package:http/http.dart' as http;

import '../app/app.logger.dart';

@lazySingleton
class Api{
  static const baseUrl = 'https://cuba-cmt-alpha.cmtelematics.com/cuba/login_step1';
  static const cubaloginurl = 'https://cuba-cmt-alpha.cmtelematics.com/cuba/login_step2';
  static const applicationId = 'com.cmtelematics.gemini.dashcam.dev';
  static const redirectBase = ".uberauth://redirect";

  final log = getLogger('Api');
  Future<dynamic> getAccountDetails(String code) async{
    var redirect = applicationId+redirectBase;
    var body = '';
    final response = await http.post(
      Uri.parse(cubaloginurl),
      headers: <String, String>{
        'Content-Type': 'application/json',
        'X-OAuth-Authorization-Code': code,
        'X-OAuth-Redirect':redirect,
      },
      body: jsonEncode(body),
    );
    if (response.statusCode == 200) {
      return CubaLoginResponse.fromJson(response.body);
    }
    else{
      throw Exception("failed to login");
    }
  }


}