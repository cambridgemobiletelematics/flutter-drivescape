import 'package:flutter/material.dart';
import 'package:geminiapp/res/customcolors.dart';
import 'package:geminiapp/res/gradient.dart';
import 'package:geminiapp/utils/utility.dart';
import 'package:geminiapp/view/sizeconfig.dart';
import 'package:geminiapp/view_model/incidentdetails_viewmodel.dart';
import 'package:stacked/stacked.dart';
import 'package:vidplayer/vidplayer.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class IncidentDetailsView extends StatelessWidget {
  final int? indexValue;

  const IncidentDetailsView({Key? key, this.indexValue}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<IncidentDetailsViewModel>.reactive(
      viewModelBuilder: () => IncidentDetailsViewModel(),
      onModelReady: (model) => model.initState(indexValue!),
      disposeViewModel: true,
      builder: (context, viewModel, child) {
        return Scaffold(
          body: Stack(children: [
            Container(
                decoration:
                    BoxDecoration(gradient: CustomGradient.linearGradient),
                height: MediaQuery.of(context).size.height),
            Padding(
              padding:  EdgeInsets.only(left: SizeConfig.screenwidth*0.25, top: SizeConfig.screenheight*0.07),
              child: Text(
                Utility.getDate(
                    viewModel.IncidentDetailsInfo.createdDate.toString()),
                style: const TextStyle(
                  decoration: TextDecoration.none,
                  fontSize: 32,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.only( left: SizeConfig.screenwidth*0.05,top: SizeConfig.screenheight*0.065),
                child: IconButton(
                  iconSize: 35,
                  color: Colors.white,
                  icon: const BackButtonIcon(),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                )),
            Positioned(
              top: SizeConfig.screenheight*0.13,
              left: SizeConfig.screenwidth*0.075,
              right: 0,
              child: Text(
                viewModel.getLocation(),
                style: const TextStyle(
                    fontWeight: FontWeight.normal,
                    color: Colors.white,
                    fontSize: 15),
              ),
            ),
            Positioned(
              top: SizeConfig.screenheight*0.175,
              left: SizeConfig.screenwidth*0.095,
              right: 0,
              child: Text(
                viewModel.getTrigger(),
                style: TextStyle(
                    background: Paint()
                      ..color = CustomColors.cardImgBackground
                      ..strokeWidth = 18
                      ..strokeCap = StrokeCap.round
                      ..strokeJoin = StrokeJoin.round
                      ..style = PaintingStyle.stroke,
                    fontWeight: FontWeight.normal),
              ),
            ),
             Positioned(
               top:  SizeConfig.screenheight*0.225,
              left: 0,
              right: 0,
              child: Divider(
                indent:SizeConfig.screenwidth*0.065,
                endIndent:SizeConfig.screenwidth*0.065,
                thickness: 2,
                color: Colors.white,
              ),
            ),
            Positioned(
              top: SizeConfig.screenheight*0.25,
              left: SizeConfig.screenwidth*0.065,
              right: 0,
              child: Text(
                AppLocalizations.of(context)!.roadCamera,
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: Colors.white,
                    fontSize: 20),
              ),
            ),
            Positioned(
                top: SizeConfig.screenheight*0.3,
                left: SizeConfig.screenwidth*0.065,
                right: SizeConfig.screenwidth*0.065,
                bottom: SizeConfig.screenheight*0.5,
                child: viewModel.customWidgetRoad()),
            Positioned(
              top: SizeConfig.screenheight*0.55,
              left:  SizeConfig.screenwidth*0.065,
              right: 0,
              child: Text(
                AppLocalizations.of(context)!.cabinCamera,
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    color: Colors.white,
                    fontSize: 20),
              ),
            ),
            Positioned(
                top:  SizeConfig.screenheight*0.6,
                left: SizeConfig.screenwidth*0.065,
                right: SizeConfig.screenwidth*0.065,
                bottom:  SizeConfig.screenheight*0.2,
                child: viewModel.customWidgetCabin()),
          ]),
        );
      },
    );
  }
}
