import 'package:flutter/material.dart';
import 'package:geminiapp/view/drivescape.dart';
import 'package:geminiapp/view/setup_dialog_ui.dart';
import 'app/app.locator.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  setupDialogUi();
  //runApp(const UberDriveScape());
  runApp(const DriveScape());
}
