import 'package:geminiapp/app/app.locator.dart';
import 'package:geminiapp/app/app.logger.dart';
import 'package:geminiapp/models/incidentlistmodel.dart';
import 'package:geminiapp/services/sdkservices.dart';
import 'package:geminiapp/services/vehiclesservices.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class IncidentListServices {
  final log = getLogger('IncidentListServices');
  late IncidentListModel _incidentList;

  IncidentListModel get IncidentListResponse => _incidentList;
  final _sdkService = locator<SDKServices>();
  final VehiclesModelServices _vehiclesModelServices =
      locator<VehiclesModelServices>();

  Future<dynamic> getIncidentList() async {
    try {
      if (_vehiclesModelServices.VehicleResponse.vehicles.isNotEmpty) {
        //TODO check mac addrees are present
        if(_vehiclesModelServices.VehicleResponse.vehicles[0].tag_mac_address != null )
          {
            var response = await _sdkService.sdkGetIncidentList(
                _vehiclesModelServices
                    .VehicleResponse.vehicles[0].tag_mac_address!);
            log.d("response :$response");
            _incidentList = IncidentListModel.fromJson(response);

            return _incidentList;
          }else
            {
              return "null";
            }

      }
      {
        return "null";
      }
    } catch (e) {
      log.d("Error Message :$e");
      return "error Message" + e.toString();
    }
  }
}
