import 'package:flutter/material.dart';
import 'package:geminiapp/res/customcolors.dart';
import 'package:geminiapp/res/gradient.dart';
import 'package:geminiapp/utils/utility.dart';
import 'package:geminiapp/view/sizeconfig.dart';
import 'package:geminiapp/view_model/allvideos_viewmodel.dart';
import 'package:stacked/stacked.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AllVideosView extends StatelessWidget {
  const AllVideosView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<AllVideosViewModel>.reactive(
      disposeViewModel: false,
      initialiseSpecialViewModelsOnce: true,
      viewModelBuilder: () => AllVideosViewModel(),
      onModelReady: (model) => model.getaccount(),
      builder: (context, viewModel, child) {
        return Scaffold(
          body: Stack(
            children: [
              Container(
                  decoration:
                      BoxDecoration(gradient: CustomGradient.linearGradient),
                  height: MediaQuery.of(context).size.height),
              Positioned(
                top: SizeConfig.screenheight * .09,
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: SizeConfig.screenwidth * 0.1),
                  child: Text(
                    AppLocalizations.of(context)!.allVideos,
                    style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 25),
                  ),
                ),
              ),
              //add menu on top right with positioned
              Positioned(
                top: SizeConfig.screenheight * .18,
                left: 0,
                right: 0,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  decoration: const BoxDecoration(
                      color: CustomColors.colorBackground,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      )),
                ),
              ),
              Positioned(
                  top: SizeConfig.screenheight * .20,
                  left: 0,
                  right: 0,
                  bottom: 0,
                  child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: viewModel.incidentList.length,
                      itemBuilder: (context, index) {
                        return SizedBox(
                            height: 170,
                            child: Card(
                              elevation: 2,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15.0)),
                              color: Colors.white,
                              margin: const EdgeInsets.symmetric(
                                horizontal: 28,
                                vertical: 10,
                              ),
                              child: GestureDetector(
                                  onTap: () {
                                    viewModel.navigateToIncidentDetails(index);
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              const SizedBox(
                                                height: 8,
                                              ),
                                              Text(
                                                Utility.getDate(viewModel
                                                        .incidentList[index]
                                                        .createdDate ??
                                                    ""),
                                                style: const TextStyle(
                                                    fontWeight: FontWeight.w500,
                                                    color: CustomColors
                                                        .colorPrimary),
                                              ),
                                              const SizedBox(
                                                height: 8,
                                              ),
                                              Text(
                                                Utility.getTime(viewModel
                                                        .incidentList[index]
                                                        .createdDate ??
                                                    ""),
                                                style: const TextStyle(
                                                    fontWeight:
                                                        FontWeight.w400),
                                              ),
                                              const SizedBox(
                                                height: 8,
                                              ),
                                              Text(
                                                viewModel.getLocation(index),
                                                style: const TextStyle(
                                                    fontWeight:
                                                        FontWeight.w400),
                                              ),
                                              const SizedBox(
                                                height: 15,
                                              ),
                                              Text(
                                                viewModel.getTrigger(index),
                                                style: TextStyle(
                                                    background: Paint()
                                                      ..color = CustomColors
                                                          .cardImgBackground
                                                      ..strokeWidth = 18
                                                      ..strokeCap =
                                                          StrokeCap.round
                                                      ..strokeJoin =
                                                          StrokeJoin.round
                                                      ..style =
                                                          PaintingStyle.stroke,
                                                    fontWeight:
                                                        FontWeight.w400),
                                              ),
                                            ]),
                                        Column(
                                          children: [
                                            const SizedBox(
                                              height: 25,
                                            ),
                                            Container(
                                              width: 100,
                                              height: 80,
                                              decoration: const BoxDecoration(
                                                  color: CustomColors
                                                      .cardImgBackground,
                                                  borderRadius:
                                                      BorderRadius.all(
                                                    Radius.circular(8.0),
                                                  )),
                                              child: Image.asset(
                                                viewModel.getUserInitiatedImage(
                                                    index),
                                                height: 100,
                                                width: 100,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  )),
                            ));
                      }))
            ],
          ),
        );
      },
    );
  }
}
