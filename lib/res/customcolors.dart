import 'dart:ui';

class CustomColors {
  static const Color lightPurple = Color(0xFFBB86FA);
  static const Color purple = Color(0xFF6002EE);
  static const Color deepPurple = Color(0xFF3900B1);
  static const Color grey = Color(0xFFC1BBBB);
  static const Color darkGrey = Color(0xFF222222);
  static const Color black = Color(0xFF141414);
  static const Color colorPrimaryDark = Color(0xFF134C7B);
  static const Color colorPrimary = Color(0xFF1C77C3);
  static const Color colorBackground = Color(0xFFF5F7FA);
  static const Color cardImgBackground = Color(0XFFE3F3F2);
  static const Color appBackground = Color(0XFFFFFFFF);
}
