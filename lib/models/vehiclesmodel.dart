import 'dart:convert';

import 'package:flutter/foundation.dart';

import 'package:geminiapp/models/vehiclemodel.dart';

class VehiclesModels {
  List<VehicleModel> vehicles;
  VehiclesModels({
    required this.vehicles,
  });

  VehiclesModels copyWith({
    List<VehicleModel>? vehicles,
  }) {
    return VehiclesModels(
      vehicles: vehicles ?? this.vehicles,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'vehicles': vehicles.map((x) => x.toMap()).toList(),
    };
  }

  factory VehiclesModels.fromMap(Map<String, dynamic> map) {
    return VehiclesModels(
      vehicles: List<VehicleModel>.from(
          map['vehicles']?.map((x) => VehicleModel.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory VehiclesModels.fromJson(String source) =>
      VehiclesModels.fromMap(json.decode(source));

  @override
  String toString() => 'VehiclesModels(vehicles: $vehicles)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is VehiclesModels && listEquals(other.vehicles, vehicles);
  }

  @override
  int get hashCode => vehicles.hashCode;
}
