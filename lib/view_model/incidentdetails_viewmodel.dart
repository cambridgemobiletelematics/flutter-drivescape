import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:geminiapp/app/app.logger.dart';
import 'package:geminiapp/models/deviceparams.dart';
import 'package:geminiapp/models/incidentmodel.dart';
import 'package:geminiapp/services/Incidentdetailsservices.dart';
import 'package:geminiapp/services/incidentservices.dart';
import 'package:stacked/stacked.dart';
import 'package:vidplayer/vidplayer.dart';

import '../app/app.locator.dart';

class IncidentDetailsViewModel extends BaseViewModel {
  final log = getLogger('IncidentDetailsViewModel');
  final IncidentListServices _incidentListServices =
      locator<IncidentListServices>();

  final IncidentDetails _incidentDetails = locator<IncidentDetails>();

  late IncidentModel _incidentModel;
  IncidentModel get IncidentDetailsInfo => _incidentModel;
  late DeviceParams _deviceParams;

  void initState(int indexval) async {
    _deviceParams = _incidentDetails.deviceParams;
    log.d('index $indexval');
    int indexd = 0;
    if (indexval != null) {
      indexd = indexval;
    }
    _incidentModel =
        _incidentListServices.IncidentListResponse.incidentList[indexd];
    var url = _incidentModel.links?.roadVideo;
    if (url != null) {}
  }

  String getLocation() {
    if (_incidentModel.city != null || _incidentModel.state != null) {
      return 'Location ' + _incidentModel.city! + ' ' + _incidentModel.state!;
    } else {
      return "Location UNKNOWN";
    }
  }

  String getTrigger() {
    if (_incidentModel.source == Source.EXT_BUTTON) {
      return 'Button Press';
    } else if (_incidentModel.source == Source.HIGH_ACC) {
      return 'Hard brake';
    } else if (_incidentModel.source != null) {
      return _incidentModel.source.toString();
    }
    return 'UNKNOWN';
  }

  String setHeaders() {
    var apikey = _deviceParams.AppKey;
    var sessionid = _deviceParams.SessionId;
    var version = _deviceParams.AppVersion;
    var deviceid = _deviceParams.DeviceId;
    var userid = _incidentModel.userId;

    var mapData = <String, String>{
      "X-Cmt-Api-Key": "$apikey",
      "X-Cmt-Session-id": "$sessionid",
      "X-Cmt-Version": "$version",
      "X-Cmt-Deviceid": "$deviceid",
      "X-Cmt-Userid": "$userid"
    };

    return json.encode(mapData);
  }

  Widget customWidgetRoad() {
    var test = getRoadUrl();
    if (test != "false") {
      return YoYoPlayer(
        aspectRatio: 16 / 9,
        url: getRoadUrl(),
        videoStyle: VideoStyle(),
        videoLoadingStyle: VideoLoadingStyle(
          loading: const Center(
            child: Text("Loading video"),
          ),
        ),
        headers: setHeaders(),
      );
    } else {
      return const Text("No Videos");
    }
  }

  Widget customWidgetCabin() {
    var test = getCabinUrl();
    if (test != "false") {
      return YoYoPlayer(
        aspectRatio: 16 / 9,
        url: getCabinUrl(),
        videoStyle: VideoStyle(),
        videoLoadingStyle: VideoLoadingStyle(
          loading: const Center(
            child: Text("Loading video"),
          ),
        ),
        headers: setHeaders(),
      );
    } else {
      return Center(child: const Text("No Videos"));
    }
  }

  String getRoadUrl() {
    var baseurl = _deviceParams.endPoint;
    var roadurl = _incidentModel.links!.roadVideo;
    log.d("$baseurl, $roadurl");
    if (roadurl != null) {
      return baseurl! + roadurl;
    } else {
      return "false";
    }
  }

  String getCabinUrl() {
    var baseurl = _deviceParams.endPoint;
    var cabinurl = _incidentModel.links!.cabinVideo;
    log.d("$baseurl, $cabinurl");
    if (cabinurl != null) {
      return baseurl! + cabinurl;
    } else {
      return "false";
    }
  }
}
