import 'dart:convert';

class AnnotatedRoadVideos {
  String? tailgating;
  String? laneMerge;
  AnnotatedRoadVideos({
    this.tailgating,
    this.laneMerge,
  });

  AnnotatedRoadVideos copyWith({
    String? tailgating,
    String? laneMerge,
  }) {
    return AnnotatedRoadVideos(
      tailgating: tailgating ?? this.tailgating,
      laneMerge: laneMerge ?? this.laneMerge,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'tailgating': tailgating,
      'lane_merge': laneMerge,
    };
  }

  factory AnnotatedRoadVideos.fromMap(Map<String, dynamic> map) {
    return AnnotatedRoadVideos(
      tailgating: map['tailgating'],
      laneMerge: map['lane_merge'],
    );
  }

  String toJson() => json.encode(toMap());

  factory AnnotatedRoadVideos.fromJson(String source) =>
      AnnotatedRoadVideos.fromMap(json.decode(source));

  @override
  String toString() =>
      'AnnotatedRoadVideos(tailgating: $tailgating, laneMerge: $laneMerge)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is AnnotatedRoadVideos &&
        other.tailgating == tailgating &&
        other.laneMerge == laneMerge;
  }

  @override
  int get hashCode => tailgating.hashCode ^ laneMerge.hashCode;
}

class AnnotatedCabinVideos {
  String? drowsiness;
  String? distracted;
  AnnotatedCabinVideos({
    this.drowsiness,
    this.distracted,
  });

  AnnotatedCabinVideos copyWith({
    String? drowsiness,
    String? distracted,
  }) {
    return AnnotatedCabinVideos(
      drowsiness: drowsiness ?? this.drowsiness,
      distracted: distracted ?? this.distracted,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'drowsiness': drowsiness,
      'distracted': distracted,
    };
  }

  factory AnnotatedCabinVideos.fromMap(Map<String, dynamic> map) {
    return AnnotatedCabinVideos(
      drowsiness: map['drowsiness'],
      distracted: map['distracted'],
    );
  }

  String toJson() => json.encode(toMap());

  factory AnnotatedCabinVideos.fromJson(String source) =>
      AnnotatedCabinVideos.fromMap(json.decode(source));

  @override
  String toString() =>
      'AnnotatedCabinVideos(drowsiness: $drowsiness, distracted: $distracted)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is AnnotatedCabinVideos &&
        other.drowsiness == drowsiness &&
        other.distracted == distracted;
  }

  @override
  int get hashCode => drowsiness.hashCode ^ distracted.hashCode;
}

class Links {
  //"cabin_snapshot"
  String? cabinSnapShot;
  //cabin_video
  String? cabinVideo;
  //road_video
  String? roadVideo;
  String? cabinVideoThumbnail;
  String? roadVideoThumbnail;
  String? detail;
  AnnotatedRoadVideos? annotatedRoadVideos;
  AnnotatedCabinVideos? annotatedCabinVideos;
  String? videoLabels;
  Links({
    this.cabinSnapShot,
    this.cabinVideo,
    this.roadVideo,
    this.cabinVideoThumbnail,
    this.roadVideoThumbnail,
    this.detail,
    this.annotatedRoadVideos,
    this.annotatedCabinVideos,
    this.videoLabels,
  });

  Links copyWith({
    String? cabinSnapShot,
    String? cabinVideo,
    String? roadVideo,
    String? cabinVideoThumbnail,
    String? roadVideoThumbnail,
    String? detail,
    AnnotatedRoadVideos? annotatedRoadVideos,
    AnnotatedCabinVideos? annotatedCabinVideos,
    String? videoLabels,
  }) {
    return Links(
      cabinSnapShot: cabinSnapShot ?? this.cabinSnapShot,
      cabinVideo: cabinVideo ?? this.cabinVideo,
      roadVideo: roadVideo ?? this.roadVideo,
      cabinVideoThumbnail: cabinVideoThumbnail ?? this.cabinVideoThumbnail,
      roadVideoThumbnail: roadVideoThumbnail ?? this.roadVideoThumbnail,
      detail: detail ?? this.detail,
      annotatedRoadVideos: annotatedRoadVideos ?? this.annotatedRoadVideos,
      annotatedCabinVideos: annotatedCabinVideos ?? this.annotatedCabinVideos,
      videoLabels: videoLabels ?? this.videoLabels,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'cabin_snapshot': cabinSnapShot,
      'cabin_video': cabinVideo,
      'road_video': roadVideo,
      'cabin_video_thumbnail': cabinVideoThumbnail,
      'road_video_thumbnail': roadVideoThumbnail,
      'detail': detail,
      'annotated_road_videos': annotatedRoadVideos?.toMap(),
      'annotated_cabin_videos': annotatedCabinVideos?.toMap(),
      'video_labels': videoLabels,
    };
  }

  factory Links.fromMap(Map<String, dynamic> map) {
    return Links(
      cabinSnapShot: map['cabin_snapshot'],
      cabinVideo: map['cabin_video'],
      roadVideo: map['road_video'],
      cabinVideoThumbnail: map['cabin_video_thumbnail'],
      roadVideoThumbnail: map['road_video_thumbnail'],
      detail: map['detail'],
      annotatedRoadVideos: map['annotated_road_videos'] != null
          ? AnnotatedRoadVideos.fromMap(map['annotated_road_videos'])
          : null,
      annotatedCabinVideos: map['annotated_cabin_videos'] != null
          ? AnnotatedCabinVideos.fromMap(map['annotated_cabin_videos'])
          : null,
      videoLabels: map['video_labels'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Links.fromJson(String source) => Links.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Links(cabinSnapShot: $cabinSnapShot, cabinVideo: $cabinVideo, roadVideo: $roadVideo, cabinVideoThumbnail: $cabinVideoThumbnail, roadVideoThumbnail: $roadVideoThumbnail, detail: $detail, annotatedRoadVideos: $annotatedRoadVideos, annotatedCabinVideos: $annotatedCabinVideos, videoLabels: $videoLabels)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Links &&
        other.cabinSnapShot == cabinSnapShot &&
        other.cabinVideo == cabinVideo &&
        other.roadVideo == roadVideo &&
        other.cabinVideoThumbnail == cabinVideoThumbnail &&
        other.roadVideoThumbnail == roadVideoThumbnail &&
        other.detail == detail &&
        other.annotatedRoadVideos == annotatedRoadVideos &&
        other.annotatedCabinVideos == annotatedCabinVideos &&
        other.videoLabels == videoLabels;
  }

  @override
  int get hashCode {
    return cabinSnapShot.hashCode ^
        cabinVideo.hashCode ^
        roadVideo.hashCode ^
        cabinVideoThumbnail.hashCode ^
        roadVideoThumbnail.hashCode ^
        detail.hashCode ^
        annotatedRoadVideos.hashCode ^
        annotatedCabinVideos.hashCode ^
        videoLabels.hashCode;
  }
}
