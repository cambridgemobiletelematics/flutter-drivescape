import 'dart:convert';

class DeviceParams {
  String? DeviceId;
  String? AppVersion;
  String? AppKey;
  String? SessionId;
  String? endPoint;
  DeviceParams({
    this.DeviceId,
    this.AppVersion,
    this.AppKey,
    this.SessionId,
    this.endPoint,
  });

  DeviceParams copyWith({
    String? DeviceId,
    String? AppVersion,
    String? AppKey,
    String? SessionId,
    String? endPoint,
  }) {
    return DeviceParams(
      DeviceId: DeviceId ?? this.DeviceId,
      AppVersion: AppVersion ?? this.AppVersion,
      AppKey: AppKey ?? this.AppKey,
      SessionId: SessionId ?? this.SessionId,
      endPoint: endPoint ?? this.endPoint,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'DeviceId': DeviceId,
      'AppVersion': AppVersion,
      'AppKey': AppKey,
      'SessionId': SessionId,
      'endPoint': endPoint,
    };
  }

  factory DeviceParams.fromMap(Map<String, dynamic> map) {
    return DeviceParams(
      DeviceId: map['DeviceId'],
      AppVersion: map['AppVersion'],
      AppKey: map['AppKey'],
      SessionId: map['SessionId'],
      endPoint: map['endPoint'],
    );
  }

  String toJson() => json.encode(toMap());

  factory DeviceParams.fromJson(String source) =>
      DeviceParams.fromMap(json.decode(source));

  @override
  String toString() {
    return 'DeviceParams(DeviceId: $DeviceId, AppVersion: $AppVersion, AppKey: $AppKey, SessionId: $SessionId, endPoint: $endPoint)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is DeviceParams &&
        other.DeviceId == DeviceId &&
        other.AppVersion == AppVersion &&
        other.AppKey == AppKey &&
        other.SessionId == SessionId &&
        other.endPoint == endPoint;
  }

  @override
  int get hashCode {
    return DeviceId.hashCode ^
        AppVersion.hashCode ^
        AppKey.hashCode ^
        SessionId.hashCode ^
        endPoint.hashCode;
  }
}
