import 'package:geminiapp/app/app.locator.dart';
import 'package:geminiapp/app/app.logger.dart';
import 'package:geminiapp/services/sdkservices.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class MediaDataServices {
  final log = getLogger('MediaDataServices');
  final _sdkService = locator<SDKServices>();

  Future<String> getMediaData(String url) async {
    try {
      var result = await _sdkService.sdkGetMediaData(url);
      return result as String;
    }
    catch (e) {
      log.d("Error Message :${e}");
      return "error Message" + e.toString();
    }
  }
}