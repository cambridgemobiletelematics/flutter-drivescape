// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// StackedRouterGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked/stacked_annotations.dart';

import '../view/allvideos_view.dart';
import '../view/camerasettings_view.dart';
import '../view/contactsupport_view.dart';
import '../view/dashboard_view.dart';
import '../view/faq_view.dart';
import '../view/feedback_view.dart';
import '../view/help_view.dart';
import '../view/home_view.dart';
import '../view/incident_details_view.dart';
import '../view/login_view.dart';
import '../view/loginpin_view.dart';
import '../view/needhelp_ledlights_view.dart';
import '../view/uberlogin_view.dart';
import '../view/welcome_uber_view.dart';

class Routes {
  static const String welcomeUberView = '/';
  static const String uberLoginView = '/uber-login-view';
  static const String allVideosView = '/all-videos-view';
  static const String incidentDetailsView = '/incident-details-view';
  static const String homeView = '/home-view';
  static const String dashboardView = '/dashboard-view';
  static const String needhelp_Ledlights_View = '/needhelp_-ledlights_-view';
  static const String feedback_View = '/feedback_-view';
  static const String contactsupport_View = '/contactsupport_-view';
  static const String help_View = '/help_-view';
  static const String faq_View = '/faq_-view';
  static const String camerasettings_View = '/camerasettings_-view';
  static const String loginPinView = '/login-pin-view';
  static const String loginView = '/login-view';
  static const all = <String>{
    welcomeUberView,
    uberLoginView,
    allVideosView,
    incidentDetailsView,
    homeView,
    dashboardView,
    needhelp_Ledlights_View,
    feedback_View,
    contactsupport_View,
    help_View,
    faq_View,
    camerasettings_View,
    loginPinView,
    loginView,
  };
}

class StackedRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.welcomeUberView, page: WelcomeUberView),
    RouteDef(Routes.uberLoginView, page: UberLoginView),
    RouteDef(Routes.allVideosView, page: AllVideosView),
    RouteDef(Routes.incidentDetailsView, page: IncidentDetailsView),
    RouteDef(Routes.homeView, page: HomeView),
    RouteDef(Routes.dashboardView, page: DashboardView),
    RouteDef(Routes.needhelp_Ledlights_View, page: Needhelp_Ledlights_View),
    RouteDef(Routes.feedback_View, page: Feedback_View),
    RouteDef(Routes.contactsupport_View, page: Contactsupport_View),
    RouteDef(Routes.help_View, page: Help_View),
    RouteDef(Routes.faq_View, page: Faq_View),
    RouteDef(Routes.camerasettings_View, page: Camerasettings_View),
    RouteDef(Routes.loginPinView, page: LoginPinView),
    RouteDef(Routes.loginView, page: LoginView),
  ];
  @override
  Map<Type, StackedRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, StackedRouteFactory>{
    WelcomeUberView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const WelcomeUberView(),
        settings: data,
      );
    },
    UberLoginView: (data) {
      var args = data.getArgs<UberLoginViewArguments>(
        orElse: () => UberLoginViewArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => UberLoginView(key: args.key),
        settings: data,
      );
    },
    AllVideosView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const AllVideosView(),
        settings: data,
      );
    },
    IncidentDetailsView: (data) {
      var args = data.getArgs<IncidentDetailsViewArguments>(
        orElse: () => IncidentDetailsViewArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => IncidentDetailsView(
          key: args.key,
          indexValue: args.indexValue,
        ),
        settings: data,
      );
    },
    HomeView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const HomeView(),
        settings: data,
      );
    },
    DashboardView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const DashboardView(),
        settings: data,
      );
    },
    Needhelp_Ledlights_View: (data) {
      var args = data.getArgs<Needhelp_Ledlights_ViewArguments>(
        orElse: () => Needhelp_Ledlights_ViewArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => Needhelp_Ledlights_View(key: args.key),
        settings: data,
      );
    },
    Feedback_View: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const Feedback_View(),
        settings: data,
      );
    },
    Contactsupport_View: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const Contactsupport_View(),
        settings: data,
      );
    },
    Help_View: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const Help_View(),
        settings: data,
      );
    },
    Faq_View: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const Faq_View(),
        settings: data,
      );
    },
    Camerasettings_View: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => const Camerasettings_View(),
        settings: data,
      );
    },
    LoginPinView: (data) {
      var args = data.getArgs<LoginPinViewArguments>(
        orElse: () => LoginPinViewArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => LoginPinView(key: args.key),
        settings: data,
      );
    },
    LoginView: (data) {
      var args = data.getArgs<LoginViewArguments>(
        orElse: () => LoginViewArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => LoginView(key: args.key),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Arguments holder classes
/// *************************************************************************

/// UberLoginView arguments holder class
class UberLoginViewArguments {
  final Key? key;
  UberLoginViewArguments({this.key});
}

/// IncidentDetailsView arguments holder class
class IncidentDetailsViewArguments {
  final Key? key;
  final int? indexValue;
  IncidentDetailsViewArguments({this.key, this.indexValue});
}

/// Needhelp_Ledlights_View arguments holder class
class Needhelp_Ledlights_ViewArguments {
  final Key? key;
  Needhelp_Ledlights_ViewArguments({this.key});
}

/// LoginPinView arguments holder class
class LoginPinViewArguments {
  final Key? key;
  LoginPinViewArguments({this.key});
}

/// LoginView arguments holder class
class LoginViewArguments {
  final Key? key;
  LoginViewArguments({this.key});
}
