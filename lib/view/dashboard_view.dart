import 'package:flutter/material.dart';
import 'package:geminiapp/res/customcolors.dart';
import 'package:geminiapp/res/gradient.dart';
import 'package:geminiapp/utils/utility.dart';
import 'package:geminiapp/view/sizeconfig.dart';
import 'package:geminiapp/view_model/dashboard_viewmodel.dart';
import 'package:stacked/stacked.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class DashboardView extends StatelessWidget {
  const DashboardView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ViewModelBuilder<DashboardViewModel>.reactive(
        disposeViewModel: false,
        initialiseSpecialViewModelsOnce: true,
        viewModelBuilder: () => DashboardViewModel(),
        onModelReady: (model) => model.onLoad(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: Stack(children: [
              Container(
                decoration:
                    BoxDecoration(gradient: CustomGradient.linearGradient),
              ),
              Positioned(
                  top: SizeConfig.screenheight * .09,
                  left: 0,
                  right: 0,
                  child: Image.asset(
                    "assets/images/drivescapelogo.png",
                    height: SizeConfig.screenheight * .0575,
                  )),
              Positioned(
                top: SizeConfig.screenheight * .18,
                left: 0,
                right: 0,
                child: Container(
                  width: SizeConfig.screenwidth,
                  height: SizeConfig.screenheight,
                  decoration: const BoxDecoration(
                      color: CustomColors.colorBackground,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      )),
                ),
              ),
              Positioned(
                  top: SizeConfig.screenheight * .22,
                  left: SizeConfig.screenwidth * .08,
                  right: 0,
                  child: Text(
                    AppLocalizations.of(context)!.yourVideos,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.screenwidth * 0.05,
                    ),
                  )),
              Positioned(
                  top: SizeConfig.screenheight * .20,
                  right: SizeConfig.screenwidth * .08,
                  child: TextButton(
                    onPressed: () => viewModel.navigateToAllVideos(),
                    child: Text(
                      AppLocalizations.of(context)!.viewAll,
                      style: TextStyle(
                        color: CustomColors.colorPrimary,
                        fontWeight: FontWeight.normal,
                        fontSize: SizeConfig.screenwidth * 0.04,
                      ),
                    ),
                  )),
              Positioned(
                top: SizeConfig.screenheight * 0.26,
                left: 0,
                right: 0,
                // bottom: SizeConfig.screenwidth * 0.6,
                child: SizedBox(
                  width: SizeConfig.screenwidth,
                  height: SizeConfig.screenheight * 0.35,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemCount: viewModel.incidentList.length,
                      itemBuilder: (context, index) {
                        return SizedBox(
                          //height: SizeConfig.screenheight * 0.8,
                          width: SizeConfig.screenheight * 0.30,
                          child: Card(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0)),
                              color: Colors.white,
                              margin: EdgeInsets.symmetric(
                                horizontal: SizeConfig.screenheight * 0.01,
                                vertical: SizeConfig.screenheight * 0.01,
                              ),
                              child: GestureDetector(
                                onTap: () {
                                  viewModel.navigateToIncidentDetails(index);
                                },
                                child: Padding(
                                  padding: EdgeInsets.all(
                                      SizeConfig.screenheight * 0.01),
                                  child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          width: SizeConfig.screenheight * 0.28,
                                          height:
                                              SizeConfig.screenheight * 0.15,
                                          decoration: const BoxDecoration(
                                              color: CustomColors
                                                  .cardImgBackground,
                                              borderRadius: BorderRadius.all(
                                                Radius.circular(3.0),
                                              )),
                                          child: Image.asset(
                                            viewModel
                                                .getUserInitiatedImage(index),
                                            fit: BoxFit.fill,
                                          ),
                                        ),
                                        SizedBox(
                                          height:
                                              SizeConfig.screenheight * 0.01,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 12.0),
                                          child: Text(
                                            Utility.getDate(viewModel
                                                    .incidentList[index]
                                                    .createdDate ??
                                                ""),
                                            style: TextStyle(
                                                fontWeight: FontWeight.w600,
                                                color:
                                                    CustomColors.colorPrimary,
                                                fontSize:
                                                    SizeConfig.screenheight *
                                                        0.0175),
                                          ),
                                        ),
                                        SizedBox(
                                          height:
                                              SizeConfig.screenheight * 0.005,
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 12),
                                          child: Text(
                                            Utility.getTime(viewModel
                                                    .incidentList[index]
                                                    .createdDate ??
                                                ""),
                                            style: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                fontSize:
                                                    SizeConfig.screenheight *
                                                        0.0165),
                                          ),
                                        ),
                                        SizedBox(
                                          height:
                                              SizeConfig.screenheight * 0.005,
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 12.0),
                                          child: Text(
                                            viewModel.getLocation(index),
                                            style: TextStyle(
                                                fontWeight: FontWeight.w400,
                                                fontSize:
                                                    SizeConfig.screenheight *
                                                        0.0165),
                                          ),
                                        ),
                                        SizedBox(
                                          height:
                                              SizeConfig.screenheight * 0.025,
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              bottom: SizeConfig.screenheight *
                                                  0.02,
                                              left: 20.0),
                                          child: Text(
                                            viewModel.getTrigger(index),
                                            style: TextStyle(
                                                fontSize:
                                                    SizeConfig.screenheight *
                                                        0.016,
                                                color: Colors.black,
                                                background: Paint()
                                                  ..color = CustomColors
                                                      .cardImgBackground
                                                  ..strokeWidth =
                                                      SizeConfig.screenwidth *
                                                          0.05
                                                  ..strokeCap = StrokeCap.round
                                                  ..strokeJoin =
                                                      StrokeJoin.round
                                                  ..style =
                                                      PaintingStyle.stroke,
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                      ]),
                                ),
                              )),
                        );
                      }),
                ),
              ),
              Positioned(
                  top: SizeConfig.screenheight * 0.65,
                  left: 28,
                  right: 0,
                  //bottom: SizeConfig.screenheight * 0.65,
                  child: Text(
                    AppLocalizations.of(context)!.needHelp,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: SizeConfig.screenwidth * 0.05,
                    ),
                  )),
              Positioned(
                top: SizeConfig.screenheight * 0.68,
                left: 0,
                right: 0,
                //bottom: SizeConfig.screenheight * 0.02,
                child: SizedBox(
                  width: SizeConfig.screenwidth,
                  height: SizeConfig.screenheight * 0.22,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemCount: viewModel.HelperList.length,
                      itemBuilder: (context, index) {
                        return SizedBox(
                          height: SizeConfig.screenheight * 0.12,
                          width: SizeConfig.screenwidth * 0.8,
                          child: Card(
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(15.0)),
                            color: Colors.white,
                            margin: const EdgeInsets.symmetric(
                              horizontal: 10,
                              vertical: 10,
                            ),
                            child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 4.0, horizontal: 2.0),
                                child: ListTile(
                                  title: Padding(
                                    padding: const EdgeInsets.only(left: 2.0),
                                    child: Text(
                                      viewModel.HelperList[index].title ?? "",
                                      textAlign: TextAlign.left,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          color: CustomColors.colorPrimary,
                                          fontSize:
                                              SizeConfig.screenwidth * 0.04),
                                    ),
                                  ),
                                  subtitle: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(
                                        viewModel.HelperList[index].subTitle ??
                                            "",
                                        style: TextStyle(
                                            fontSize:
                                                SizeConfig.screenwidth * 0.035),
                                      ),
                                      // SizedBox(
                                      //   height:  SizeConfig.screenwidth*0.001,
                                      // ),
                                      ElevatedButton(
                                        onPressed: () =>
                                            viewModel.navigateToFindout(index),
                                        style: ElevatedButton.styleFrom(
                                            primary: CustomColors.colorPrimary,
                                            minimumSize: Size(
                                                SizeConfig.screenwidth * 0.3,
                                                SizeConfig.screenwidth *
                                                    0.075)),
                                        child: Text(
                                          viewModel.HelperList[index]
                                                  .buttonText ??
                                              '',
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize:
                                                SizeConfig.screenwidth * 0.035,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  trailing: Image.asset(
                                    viewModel.HelperList[index].icon ?? '',
                                    width: SizeConfig.screenwidth * 0.16,
                                  ),
                                )),
                          ),
                        );
                      }),
                ),
              ),
            ]),
          );
        });
  }
}
