import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../res/customcolors.dart';

class Faq_View extends StatefulWidget {

  const Faq_View({Key? key}) : super(key: key);

  @override
  State<Faq_View> createState() => _Faq_ViewState();
}

class _Faq_ViewState extends State<Faq_View> {
  var loadingPercentage = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("FAQ"),
        backgroundColor: CustomColors.colorPrimary,
      ),
      body:  Stack(
        children: [
          WebView(
            initialUrl:
            "https://truemotion.zendesk.com/hc/en-us/articles/4409900923027-DriveScape-FAQs",
            javascriptMode: JavascriptMode.unrestricted,
            onProgress: (int progress) {
              setState(() {
                loadingPercentage = progress;
              });
              if (kDebugMode) {
                print('WebView is loading (progress : $progress%)');
              }
            },
            onPageStarted: (String url) {
              setState(() {
                loadingPercentage = 0;
              });
              if (kDebugMode) {
                print('Page started loading: $url');
              }
            },
            onPageFinished: (String url) {
              setState(() {
                loadingPercentage = 100;
              });
              if (kDebugMode) {
                print('Page finished loading: $url');
              }
            },
          ),
          Center(child: (loadingPercentage < 100)?
          const CircularProgressIndicator():null,),
        ],

      ),
    );
  }
}
