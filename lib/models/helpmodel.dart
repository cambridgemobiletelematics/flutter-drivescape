import 'dart:convert';

class HelpModel {
  String? title;
  String? subTitle;
  String? buttonText;
  String? icon;
  HelpModel({
    this.title,
    this.subTitle,
    this.buttonText,
    this.icon,
  });

  HelpModel copyWith({
    String? title,
    String? subTitle,
    String? buttonText,
    String? icon,
  }) {
    return HelpModel(
      title: title ?? this.title,
      subTitle: subTitle ?? this.subTitle,
      buttonText: buttonText ?? this.buttonText,
      icon: icon ?? this.icon,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'subTitle': subTitle,
      'buttonText': buttonText,
      'icon': icon,
    };
  }

  factory HelpModel.fromMap(Map<String, dynamic> map) {
    return HelpModel(
      title: map['title'],
      subTitle: map['subTitle'],
      buttonText: map['buttonText'],
      icon: map['icon'],
    );
  }

  String toJson() => json.encode(toMap());

  factory HelpModel.fromJson(String source) =>
      HelpModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'HelpModel(title: $title, subTitle: $subTitle, buttonText: $buttonText, icon: $icon)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is HelpModel &&
        other.title == title &&
        other.subTitle == subTitle &&
        other.buttonText == buttonText &&
        other.icon == icon;
  }

  @override
  int get hashCode {
    return title.hashCode ^
        subTitle.hashCode ^
        buttonText.hashCode ^
        icon.hashCode;
  }
}
