import 'package:flutter/material.dart';
import 'package:geminiapp/res/customcolors.dart';
import 'package:geminiapp/res/gradient.dart';
import 'package:geminiapp/view/sizeconfig.dart';
import 'package:geminiapp/view_model/feedback_viewmodel.dart';
import 'package:stacked/stacked.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Feedback_View extends StatefulWidget {
  const Feedback_View({Key? key}) : super(key: key);

  @override
  State<Feedback_View> createState() => _Feedback_ViewState();
}

class _Feedback_ViewState extends State<Feedback_View> {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<Feedback_ViewModel>.reactive(
        viewModelBuilder: () => Feedback_ViewModel(),
    builder: (context, viewModel, child) {
    return  Scaffold(resizeToAvoidBottomInset:false,
      body: Stack(
        //clipBehavior: Clip.none,
        children: [
          Container(
              decoration: BoxDecoration(
                  gradient: CustomGradient.linearGradient),
              height: MediaQuery
                  .of(context)
                  .size
                  .height),
          Positioned(
            top:SizeConfig.screenheight*0.09,
            left: 0,
            right: 0
            ,child: Padding(
            padding:  EdgeInsets.symmetric(horizontal:SizeConfig.screenwidth*0.05),
            child: Row(
              children: [
                IconButton(
                  onPressed: () {
                    Navigator.pop(
                      context,
                    );
                  },
                  icon: const Icon(Icons.arrow_back_ios_new),
                  color: Colors.white,
                ),
                const SizedBox(
                  width: 20,
                ),
                Text(
                  AppLocalizations.of(context)!.provideFeedback,
                  style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 25),
                ),
              ],
            ),
          ),),
          Positioned(
            top: SizeConfig.screenheight*0.18,
            left: 0,
            right: 0,
            child: Container(
              width: MediaQuery
                  .of(context)
                  .size
                  .width,
              height: MediaQuery
                  .of(context)
                  .size
                  .height,
              decoration: const BoxDecoration(
                  color: CustomColors.colorBackground,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  )),
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Column(
                  children: [
                    Text(
                      AppLocalizations.of(context)!.provideFeedbackMsg,
                      maxLines: 3,
                      style: const TextStyle(fontSize: 15),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: TextField(
                        minLines: 7,
                        maxLines: 10,
                        decoration: InputDecoration(
                            contentPadding: const EdgeInsets.fromLTRB(
                                5.0, 10.0, 5.0, 10.0),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            )),
                      ),
                    ),
                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                            CustomColors.colorPrimary),
                        shape: MaterialStateProperty.all<
                            RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25.0),
                          ),
                        ),
                      ),
                      onPressed: () {},
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 100.0, vertical: 15),
                        child: Text(
                          AppLocalizations.of(context)!.submit,
                          style: TextStyle(fontSize: 18),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),

        ],
      ),
    ); });
  }
}

