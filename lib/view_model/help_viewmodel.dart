
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import '../app/app.locator.dart';
import '../app/app.router.dart';

class Help_ViewModel extends BaseViewModel{

  final _navigationService = locator<NavigationService>();
  void navigateToHomeView() {
    _navigationService.navigateTo(Routes.homeView);
  }
  void navigateToFaqView(){
    _navigationService.navigateTo(Routes.faq_View);
  }
  void navigateToLedLightsView() {
    _navigationService.navigateTo(Routes.needhelp_Ledlights_View);
  }
  void navigateToContactsupport() {
    _navigationService.navigateTo(Routes.contactsupport_View);
  }
  void navigateToFeedback() {
    _navigationService.navigateTo(Routes.feedback_View);
  }
  void navigateToCamerasettings(){
    _navigationService.navigateTo(Routes.camerasettings_View);
  }
}