import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:geminiapp/view/sizeconfig.dart';
import 'package:geminiapp/view_model/help_viewmodel.dart';
import 'package:stacked/stacked.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../res/gradient.dart';
import '../res/customcolors.dart';

class Help_View extends StatefulWidget {
  const Help_View({Key? key}) : super(key: key);

  @override
  _Help_ViewState createState() => _Help_ViewState();
}

class _Help_ViewState extends State<Help_View> {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<Help_ViewModel>.reactive(
        viewModelBuilder: () => Help_ViewModel(),
        builder: (context, viewModel, child) {
          return Stack(
            //clipBehavior: Clip.none,
            children: [
              Container(
                  decoration:
                      BoxDecoration(gradient: CustomGradient.linearGradient),
                  height: MediaQuery.of(context).size.height),
              Positioned(
                top:SizeConfig.screenheight*0.09,
                child: Padding(
                    padding: EdgeInsets.symmetric( horizontal:SizeConfig.screenwidth*0.1),
                    child: Text(
                      AppLocalizations.of(context)!.help,
                      style: TextStyle(
                        decoration: TextDecoration.none,
                        fontSize: SizeConfig.screenwidth * .07,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    )),
              ),
              Positioned(
                top: SizeConfig.screenheight * .18,
                left: 0,
                right: 0,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height-SizeConfig.screenheight*.18,
                  decoration: const BoxDecoration(
                      color: CustomColors.colorBackground,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      )),
                  child: Column(children: [
                    ListView(
                      scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        children: ListTile.divideTiles(
                          context: context,
                          tiles: [
                            ListTile(
                              onTap: () => viewModel.navigateToFaqView(),
                              title: Text(AppLocalizations.of(context)!.faq,style: TextStyle(fontSize: SizeConfig.screenwidth*0.04)),
                              trailing: const Icon(Icons.arrow_forward_ios),
                            ),
                            ListTile(
                              onTap: () => viewModel.navigateToContactsupport(),
                              title: Text(AppLocalizations.of(context)!.contactSupport,style: TextStyle(fontSize: SizeConfig.screenwidth*0.04)),
                              trailing: const Icon(Icons.arrow_forward_ios),
                            ),
                            ListTile(
                              onTap: () => viewModel.navigateToCamerasettings(),
                              title: Text(AppLocalizations.of(context)!.cameraSettings,style: TextStyle(fontSize: SizeConfig.screenwidth*0.04)),
                              trailing: const Icon(Icons.arrow_forward_ios),
                            ),
                            ListTile(
                              onTap: () => viewModel.navigateToFeedback(),
                              title: Text(AppLocalizations.of(context)!.provideFeedback,style: TextStyle(fontSize: SizeConfig.screenwidth*0.04)),
                              trailing: const Icon(Icons.arrow_forward_ios),
                            ),
                            ListTile(
                              onTap: () => viewModel.navigateToLedLightsView(),
                              title: Text(AppLocalizations.of(context)!.ledLights,style: TextStyle(fontSize: SizeConfig.screenwidth*0.04)),
                              trailing: const Icon(Icons.arrow_forward_ios),
                            ),
                            ListTile(
                              onTap: () {
                                if (kDebugMode) {
                                  print("video sharing history");
                                }
                              },
                              title: Text(AppLocalizations.of(context)!.videoSharingHistory,style: TextStyle(fontSize: SizeConfig.screenwidth*0.04)),
                              trailing: const Icon(Icons.arrow_forward_ios),
                            ),
                            ListTile(
                              onTap: () {
                                if (kDebugMode) {
                                  print("about page");
                                }
                              },
                              title: Text(AppLocalizations.of(context)!.about,style: TextStyle(fontSize: SizeConfig.screenwidth*0.04)),
                              trailing: const Icon(Icons.arrow_forward_ios),
                            ),
                          ],
                        ).toList()),
                  ]),
                ),
              ),
            ],
          );
        });
  }
}
