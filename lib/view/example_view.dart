import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geminiapp/res/customcolors.dart';
import 'package:geminiapp/res/gradient.dart';

class ExampleView extends StatelessWidget {
  const ExampleView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
        overlays: SystemUiOverlay.values);
    return Scaffold(
      body: ListView(
        children: [headerAndBody(context)],
      ),
    );
  }
}

Widget headerAndBody(BuildContext context) {
  return Stack(
    //clipBehavior: Clip.none,
    children: [
      Container(
          decoration: BoxDecoration(gradient: CustomGradient.linearGradient),
          height: MediaQuery.of(context).size.height),
      HeaderPadding.headPadding,
      Positioned(
        top: 150,
        left: 0,
        right: 0,
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: const BoxDecoration(
              color: CustomColors.colorBackground,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30),
              )),
        ),
      ),
      Positioned(
        top: 180,
        left: 0,
        right: 0,
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: const BoxDecoration(
              color: CustomColors.black,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30),
                topRight: Radius.circular(30),
              )),
              child: Column(
                
              ),
        ),
      ),
    ],
  );
}
