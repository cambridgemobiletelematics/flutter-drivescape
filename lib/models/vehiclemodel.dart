import 'dart:convert';

enum TagProductType { UNKNOWN, TAG_CLASSIC, TAG_SVR, TAG_LTE, DRIVESCAPE }
enum ProvisioningState { INCOMPLETE, FAIL, SUCCESS }

class VehicleModel {
  final bool records_own_trips;
  final TagProductType? tag_product_type;
  final String? nickname;
  final String? registration;
  final String? tag_mac_address;
  final int primary_driver_short_user_id;
  final ProvisioningState? provisioning_state;
  final DateTime? tag_link_date;
  final int short_vehicle_id;
  final bool tag_is_active;
  final String? make;
  final String? model;
  VehicleModel({
    required this.records_own_trips,
    this.tag_product_type,
    this.nickname,
    this.registration,
    this.tag_mac_address,
    required this.primary_driver_short_user_id,
    this.provisioning_state,
    this.tag_link_date,
    required this.short_vehicle_id,
    required this.tag_is_active,
    this.make,
    this.model,
  });

  VehicleModel copyWith({
    bool? records_own_trips,
    TagProductType? tag_product_type,
    String? nickname,
    String? registration,
    String? tag_mac_address,
    int? primary_driver_short_user_id,
    ProvisioningState? provisioning_state,
    DateTime? tag_link_date,
    int? short_vehicle_id,
    bool? tag_is_active,
    String? make,
    String? model,
  }) {
    return VehicleModel(
      records_own_trips: records_own_trips ?? this.records_own_trips,
      tag_product_type: tag_product_type ?? this.tag_product_type,
      nickname: nickname ?? this.nickname,
      registration: registration ?? this.registration,
      tag_mac_address: tag_mac_address ?? this.tag_mac_address,
      primary_driver_short_user_id:
          primary_driver_short_user_id ?? this.primary_driver_short_user_id,
      provisioning_state: provisioning_state ?? this.provisioning_state,
      tag_link_date: tag_link_date ?? this.tag_link_date,
      short_vehicle_id: short_vehicle_id ?? this.short_vehicle_id,
      tag_is_active: tag_is_active ?? this.tag_is_active,
      make: make ?? this.make,
      model: model ?? this.model,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'records_own_trips': records_own_trips,
      'tag_product_type': tag_product_type?.index,
      'nickname': nickname,
      'registration': registration,
      'tag_mac_address': tag_mac_address,
      'primary_driver_short_user_id': primary_driver_short_user_id,
      'provisioning_state': provisioning_state?.index,
      'tag_link_date': tag_link_date,
      'short_vehicle_id': short_vehicle_id,
      'tag_is_active': tag_is_active,
      'make': make,
      'model': model,
    };
  }

  factory VehicleModel.fromMap(Map<String, dynamic> map) {
    int val = 0;
    int val1 = 0;
    DateTime dt;
    if (map.containsKey('tag_product_type')) {
      for (var element in TagProductType.values) {
        if (element.name == map['tag_product_type']) {
          val = element.index;
        }
      }
    }
    if (map.containsKey('provisioning_state')) {
      for (var element in ProvisioningState.values) {
        if (element.name == map['provisioning_state']) val1 = element.index;
      }
    }
    if (map.containsKey('tag_link_date')) {
      dt = DateTime.parse(map['tag_link_date']);
    } else {
      dt = DateTime.now();
    }

    return VehicleModel(
      records_own_trips: map['records_own_trips'] ?? false,
      tag_product_type:
          map['tag_product_type'] != null ? TagProductType.values[val] : null,
      nickname: map['nickname'],
      registration: map['registration'],
      tag_mac_address: map['tag_mac_address'],
      primary_driver_short_user_id:
          map['primary_driver_short_user_id']?.toInt() ?? 0,
      provisioning_state: map['provisioning_state'] != null
          ? ProvisioningState.values[val1]
          : null,
      tag_link_date: dt,
      short_vehicle_id: map['short_vehicle_id']?.toInt() ?? 0,
      tag_is_active: map['tag_is_active'] ?? false,
      make: map['make'],
      model: map['model'],
    );
  }

  String toJson() => json.encode(toMap());

  factory VehicleModel.fromJson(String source) =>
      VehicleModel.fromMap(json.decode(source));

  @override
  String toString() {
    return 'VehicleModel(records_own_trips: $records_own_trips, tag_product_type: $tag_product_type, nickname: $nickname, registration: $registration, tag_mac_address: $tag_mac_address, primary_driver_short_user_id: $primary_driver_short_user_id, provisioning_state: $provisioning_state, tag_link_date: $tag_link_date, short_vehicle_id: $short_vehicle_id, tag_is_active: $tag_is_active, make: $make, model: $model)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is VehicleModel &&
        other.records_own_trips == records_own_trips &&
        other.tag_product_type == tag_product_type &&
        other.nickname == nickname &&
        other.registration == registration &&
        other.tag_mac_address == tag_mac_address &&
        other.primary_driver_short_user_id == primary_driver_short_user_id &&
        other.provisioning_state == provisioning_state &&
        other.tag_link_date == tag_link_date &&
        other.short_vehicle_id == short_vehicle_id &&
        other.tag_is_active == tag_is_active &&
        other.make == make &&
        other.model == model;
  }

  @override
  int get hashCode {
    return records_own_trips.hashCode ^
        tag_product_type.hashCode ^
        nickname.hashCode ^
        registration.hashCode ^
        tag_mac_address.hashCode ^
        primary_driver_short_user_id.hashCode ^
        provisioning_state.hashCode ^
        tag_link_date.hashCode ^
        short_vehicle_id.hashCode ^
        tag_is_active.hashCode ^
        make.hashCode ^
        model.hashCode;
  }
}
