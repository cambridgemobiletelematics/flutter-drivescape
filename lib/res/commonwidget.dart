import 'package:flutter/material.dart';

import '../view/sizeconfig.dart';

BoxDecoration buildWelcomeBGBoxDecoration() {
  return const BoxDecoration(
      image: DecorationImage(
          image: AssetImage("assets/images/ic_welcome_bg_uber.jpg"),
          fit: BoxFit.cover));
}

Container buildWelcomeLogoContainer(Size size) {
  return Container(
      height: getProportionHieght(100),
      width: getProportionWidth(200),
      margin: EdgeInsets.only(top: size.height / 12),
      child: Image.asset(
        "assets/images/drivescape-home-logo.png",
        width: 370,
        height: 80,
      ));
}

Container buildCMTLogoContainer(Size size) {
  return Container(
      margin: EdgeInsets.only(top: size.height / 52),
      child: Image.asset(
        "assets/images/ic_powered_by_cmt.png",
        width: size.height/3.5,
        height: size.height/16,
      ));
}

Padding buildWelcomeTextPadding(Size size) {
  return Padding(
    padding: EdgeInsets.all(size.height / 48),
    child: const Text(
      'Your ally on the road',
      style: TextStyle(color: Colors.white, fontSize: 30),
    ),
  );
}