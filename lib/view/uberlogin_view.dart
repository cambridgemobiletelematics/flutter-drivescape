import 'package:flutter/material.dart';
import 'package:geminiapp/view_model/uberlogin_viewmodel.dart';
import 'package:stacked/stacked.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../app/app.logger.dart';

class UberLoginView extends StatelessWidget {
  final log = getLogger('UberLoginView');
  UberLoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<UberLoginViewModel>.reactive(
      viewModelBuilder: () => UberLoginViewModel(),
      onModelReady: (model) => model.initState(),
      builder: (context, viewModel, child) {
        return viewModel.isBusy ? Center(child:CircularProgressIndicator()) :WebView(
          initialUrl: viewModel.initalUrl,
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (c){
            viewModel.webviewContorller = c;
          },
          onProgress: (int progress) {
            print('WebView is loading (progress : $progress%)');
          },
          javascriptChannels: <JavascriptChannel>{
            _toasterJavascriptChannel(context),
          },
          navigationDelegate: (NavigationRequest request) {
            log.d('navigation.url :$request.url');
            log.d('redirectUrl : ' + viewModel.redirectCheck);
            if (request.url.startsWith(viewModel.redirectCheck)) {
              log.d('blocking navigation to $request}');
              viewModel.getCubeResponse(request: request);
              //viewModel.navigateToHomeView();
              return NavigationDecision.prevent;
            }
            return NavigationDecision.navigate;
          },
          onPageStarted: (String url) {
            print('Page started loading: $url');
          },
          onPageFinished: (String url) {
            print('Page finished loading: $url');
          },
          gestureNavigationEnabled: true,
          backgroundColor: Colors.black,
        );
      },
    );
  }

  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'Toaster',
        onMessageReceived: (JavascriptMessage message) {
          // ignore: deprecated_member_use
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        });
  }
}
