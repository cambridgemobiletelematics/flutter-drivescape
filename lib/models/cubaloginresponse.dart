import 'dart:convert';

class CubaLoginResponse {

  final String accountId;
  final String authCode;

  CubaLoginResponse({
    required this.accountId,
    required this.authCode,
  });

  CubaLoginResponse copyWith({
    String? accountId,
    String? authCode,
  }) {
    return CubaLoginResponse(
      accountId: accountId ?? this.accountId,
      authCode: authCode ?? this.authCode,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'accountId': accountId,
      'authCode': authCode,
    };
  }

  factory CubaLoginResponse.fromMap(Map<String, dynamic> map) {
    return CubaLoginResponse(
      accountId: map['account_id'],
      authCode: map['auth_code'],
    );
  }

  String toJson() => json.encode(toMap());

  factory CubaLoginResponse.fromJson(String source) => CubaLoginResponse.fromMap(json.decode(source));

  @override
  String toString() => 'CubaLoginResponse(accountId: $accountId, authCode: $authCode)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is CubaLoginResponse &&
      other.accountId == accountId &&
      other.authCode == authCode;
  }

  @override
  int get hashCode => accountId.hashCode ^ authCode.hashCode;
}

