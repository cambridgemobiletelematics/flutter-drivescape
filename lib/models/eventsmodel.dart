import 'dart:convert';

class Event {
  String? camera;
  String? event;
  int? offset_secs;
  int? duration_secs;
  String? created_datetime;
  Event({
    this.camera,
    this.event,
    this.offset_secs,
    this.duration_secs,
    this.created_datetime,
  });

  Event copyWith({
    String? camera,
    String? event,
    int? offset_secs,
    int? duration_secs,
    String? created_datetime,
  }) {
    return Event(
      camera: camera ?? this.camera,
      event: event ?? this.event,
      offset_secs: offset_secs ?? this.offset_secs,
      duration_secs: duration_secs ?? this.duration_secs,
      created_datetime: created_datetime ?? this.created_datetime,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'camera': camera,
      'event': event,
      'offset_secs': offset_secs,
      'duration_secs': duration_secs,
      'created_datetime': created_datetime,
    };
  }

  factory Event.fromMap(Map<String, dynamic> map) {
    return Event(
      camera: map['camera'],
      event: map['event'],
      offset_secs: map['offset_secs']?.toInt(),
      duration_secs: map['duration_secs']?.toInt(),
      created_datetime: map['created_datetime'],
    );
  }

  String toJson() => json.encode(toMap());

  factory Event.fromJson(String source) => Event.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Event(camera: $camera, event: $event, offset_secs: $offset_secs, duration_secs: $duration_secs, created_datetime: $created_datetime)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Event &&
        other.camera == camera &&
        other.event == event &&
        other.offset_secs == offset_secs &&
        other.duration_secs == duration_secs &&
        other.created_datetime == created_datetime;
  }

  @override
  int get hashCode {
    return camera.hashCode ^
        event.hashCode ^
        offset_secs.hashCode ^
        duration_secs.hashCode ^
        created_datetime.hashCode;
  }
}
