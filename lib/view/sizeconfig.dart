import 'package:flutter/material.dart';

class SizeConfig {
  static late MediaQueryData _mediaQueryData;
  static late double screenwidth;
  static late double screenheight;
  static late double defaultsize;
  static late Orientation orientation;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenheight = _mediaQueryData.size.height;
    screenwidth = _mediaQueryData.size.width;
    orientation = _mediaQueryData.orientation;
  }
}

double getProportionHieght(double inputheight) {
  double screeHeights = SizeConfig.screenheight;
  return (inputheight / 812.0) * screeHeights;
}

double getProportionWidth(double inputwidth) {
  double screenWidths = SizeConfig.screenwidth;
  return (inputwidth / 375.0) * screenWidths;
}
