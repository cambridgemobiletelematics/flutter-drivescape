import 'package:geminiapp/services/Incidentdetailsservices.dart';
import 'package:geminiapp/services/api.dart';
import 'package:geminiapp/services/helpservices.dart';
import 'package:geminiapp/services/incidentservices.dart';
import 'package:geminiapp/services/loginmodel.dart';
import 'package:geminiapp/services/mediadataservices.dart';
import 'package:geminiapp/services/sdkservices.dart';
import 'package:geminiapp/services/vehiclesservices.dart';
import 'package:geminiapp/view/camerasettings_view.dart';
import 'package:geminiapp/view/login_view.dart';
import 'package:geminiapp/view/faq_view.dart';
import 'package:geminiapp/view/feedback_view.dart';
import 'package:geminiapp/view/contactsupport_view.dart';
import 'package:geminiapp/view/help_view.dart';
import 'package:geminiapp/view/loginpin_view.dart';
import 'package:geminiapp/view/needhelp_ledlights_view.dart';
import 'package:geminiapp/view/allvideos_view.dart';
import 'package:geminiapp/view/dashboard_view.dart';
import 'package:geminiapp/view/home_view.dart';
import 'package:geminiapp/view/incident_details_view.dart';
import 'package:geminiapp/view/uberlogin_view.dart';
import 'package:geminiapp/view/welcome_uber_view.dart';
import 'package:stacked/stacked_annotations.dart';
import 'package:stacked_services/stacked_services.dart';

@StackedApp(
  routes: [
    MaterialRoute(page: WelcomeUberView, initial: true),
    MaterialRoute(page: UberLoginView),
    MaterialRoute(page: AllVideosView),
    MaterialRoute(page: IncidentDetailsView),
    MaterialRoute(page: HomeView),
    MaterialRoute(page: DashboardView),
    MaterialRoute(page: Needhelp_Ledlights_View),
    MaterialRoute(page: Feedback_View),
    MaterialRoute(page: Contactsupport_View),
    MaterialRoute(page: Help_View),
    MaterialRoute(page: Faq_View),
    MaterialRoute(page: Camerasettings_View),
    MaterialRoute(page: LoginPinView),
    MaterialRoute(page: LoginView),
  ],
  dependencies: [
    LazySingleton(classType: NavigationService),
    LazySingleton(classType: Api),
    LazySingleton(classType: LoginModelServices),
    LazySingleton(classType: VehiclesModelServices),
    LazySingleton(classType: SDKServices),
    LazySingleton(classType: IncidentListServices),
    LazySingleton(classType: MediaDataServices),
    LazySingleton(classType: HelpModelServices),
    LazySingleton(classType: IncidentDetails),
    LazySingleton(classType: DialogService),
    LazySingleton(classType: SnackbarService)
  ],
  logger: StackedLogger(),
)
class AppSetup {}
