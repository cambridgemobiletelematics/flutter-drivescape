import 'package:flutter/material.dart';

import 'package:geminiapp/res/gradient.dart';
import 'package:geminiapp/view/sizeconfig.dart';
import '../res/customcolors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Needhelp_Ledlights_View extends StatelessWidget {
  Needhelp_Ledlights_View({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
          children: [
            Container(
                decoration:
                    BoxDecoration(gradient: CustomGradient.linearGradient),
                height: MediaQuery.of(context).size.height),
            Positioned(
              top:SizeConfig.screenheight*0.09,
              child: Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal:SizeConfig.screenwidth*0.03),
                  child: Row(children: [
                    IconButton(
                      onPressed: () => Navigator.pop(context),
                      icon: const Icon(Icons.arrow_back_ios_new),
                      color: Colors.white,
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Text(
                      AppLocalizations.of(context)!.ledLights,
                      style: TextStyle(
                        decoration: TextDecoration.none,
                        fontSize: SizeConfig.screenwidth*0.07,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ])),
            ),
            Positioned(
                top: SizeConfig.screenheight*0.18,
                left: 0,
                right: 0,
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    decoration: const BoxDecoration(
                        color: CustomColors.colorBackground,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30),
                        )),
                    child: Column(children: [
                      Padding(
                          padding:  EdgeInsets.all(SizeConfig.screenwidth*0.01),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Padding(
                                  padding:   EdgeInsets.symmetric(horizontal:SizeConfig.screenwidth*0.05,vertical:SizeConfig.screenwidth*0.02),
                                  child: Row(
                                    children: [
                                      Image.asset("assets/images/roadicon.png"),
                                      const SizedBox(
                                        width: 38,
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            AppLocalizations.of(context)!.roadCamera,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w600,
                                                fontSize: SizeConfig.screenwidth*0.045),
                                          ),
                                          Container(
                                            height:  SizeConfig.screenheight*0.065,
                                            width: size.width * 2 / 3,
                                            child: Text(
                                              "Green when the camera is recording",
                                              maxLines: 2,
                                              softWrap: true,
                                              overflow: TextOverflow.fade,
                                              style: TextStyle(
                                                fontSize:SizeConfig.screenwidth*0.0435,),
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal:SizeConfig.screenwidth*0.05,vertical:SizeConfig.screenwidth*0.01 ),
                                  child: Row(
                                    children: [
                                      Image.asset(
                                          "assets/images/cabinicon.png"),
                                      const SizedBox(
                                        width: 35,
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            AppLocalizations.of(context)!.cabinCamera,
                                            style:  TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w600,
                                                fontSize:  SizeConfig.screenwidth*0.045),
                                          ),
                                          Container(
                                            height: SizeConfig.screenheight*0.065,
                                            width: size.width * 2 / 3,
                                            child: Text(
                                              AppLocalizations.of(context)!.cabinCameraMsg,
                                              maxLines: 2,
                                              softWrap: true,
                                              overflow: TextOverflow.fade,
                                              style: TextStyle(
                                                fontSize:SizeConfig.screenwidth*0.0435,))
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal:SizeConfig.screenwidth*0.05,vertical:SizeConfig.screenwidth*0.01 ),
                                  child: Row(
                                    children: [
                                      Image.asset(
                                          "assets/images/gettingreadyicon.png"),
                                      const SizedBox(
                                        width: 20,
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            AppLocalizations.of(context)!.gettingReady,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w600,
                                                fontSize:  SizeConfig.screenwidth*0.045),
                                          ),
                                          Container(
                                            height: SizeConfig.screenheight*0.065,
                                            width: size.width * 2 / 3,
                                            child: Text(
                                              AppLocalizations.of(context)!.gettingReadyMsg,
                                              maxLines: 2,
                                              softWrap: true,
                                              overflow: TextOverflow.fade,
                                              style: TextStyle(
                                                  fontSize:SizeConfig.screenwidth*0.0435),
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.symmetric(horizontal:SizeConfig.screenwidth*0.05,vertical:SizeConfig.screenwidth*0.01 ),
                                  child: Row(
                                    children: [
                                      Image.asset(
                                          "assets/images/alerticon.png"),
                                      const SizedBox(
                                        width: 25,
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            AppLocalizations.of(context)!.alert,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w600,
                                                fontSize:  SizeConfig.screenwidth*0.045),
                                          ),
                                          Container(
                                            height: SizeConfig.screenheight*0.065,
                                            width: size.width * 2 / 3,
                                            child: Text(
                                              AppLocalizations.of(context)!.alertMsg,
                                              maxLines: 2,
                                              softWrap: true,
                                              overflow: TextOverflow.fade,
                                              style: TextStyle(
                                                fontSize:SizeConfig.screenwidth*0.0435,

                                              ),
                                            ),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                                Container(
                                    margin: const EdgeInsets.all(10),
                                    child: Image.asset(
                                      "assets/images/drivescape_led_image.png",
                                      width: size.width / 1.5,
                                      height: size.width / 1.5,
                                    )),
                              ]))
                    ])))
          ]),
    );
  }
}
