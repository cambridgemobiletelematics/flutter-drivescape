import 'package:flutter/material.dart';
import 'package:geminiapp/app/app.logger.dart';
import 'package:geminiapp/res/customcolors.dart';
import 'package:geminiapp/view/sizeconfig.dart';
import 'package:geminiapp/view_model/login_viewmodel.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:stacked/stacked.dart';

class LoginView extends StatelessWidget {
  final log = getLogger('LoginView');
  LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ViewModelBuilder<LoginViewModel>.reactive(
        viewModelBuilder: () => LoginViewModel(),
        onModelReady: (model) => {},
        builder: (context, model, child) {
          return Scaffold(
            backgroundColor: CustomColors.appBackground,
            body: Container(
              padding: EdgeInsets.symmetric(
                  vertical: SizeConfig.screenheight / 8,
                  horizontal: SizeConfig.screenwidth / 7),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    AppLocalizations.of(context)!.welcome,
                    style: const TextStyle(
                        fontWeight: FontWeight.w500,
                        color: CustomColors.colorPrimary,
                        fontSize: 20),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: SizeConfig.screenheight * 0.06,
                  ),
                  Text(
                    AppLocalizations.of(context)!.enteremail,
                    style: const TextStyle(fontSize: 15),
                  ),
                  SizedBox(
                    height: SizeConfig.screenheight * 0.03,
                  ),
                  TextField(
                    controller: model.emailController,
                    decoration: InputDecoration(
                        icon: const Icon(
                          Icons.email_outlined,
                          color: Colors.blue,
                        ),
                        border: const OutlineInputBorder(),
                        hintText: AppLocalizations.of(context)!.emailaddress),
                  ),
                  SizedBox(
                    height: SizeConfig.screenheight * 0.03,
                  ),
                  ElevatedButton(
                      onPressed: () {
                        model.authentication(model.emailController.text);
                      },
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: SizeConfig.screenwidth / 25,
                            vertical: SizeConfig.screenheight / 75),
                        child: Text(
                          AppLocalizations.of(context)!.continuet,
                          style: const TextStyle(fontSize: 18),
                        ),
                      ))
                ],
              ),
            ),
          );
        });
  }
}
