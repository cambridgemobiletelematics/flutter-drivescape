import 'dart:io';
import 'package:geminiapp/models/cubaloginresponse.dart';
import 'package:geminiapp/models/incidentlistmodel.dart';
import 'package:geminiapp/models/vehiclesmodel.dart';
import 'package:geminiapp/res/constants.dart';
import 'package:geminiapp/services/api.dart';
import 'package:geminiapp/services/helpservices.dart';
import 'package:geminiapp/services/incidentservices.dart';
import 'package:geminiapp/services/loginmodel.dart';
import 'package:geminiapp/services/vehiclesservices.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:webview_flutter/webview_flutter.dart';
import '../app/app.locator.dart';
import '../app/app.logger.dart';
import '../app/app.router.dart';

class UberLoginViewModel extends BaseViewModel {
  final log = getLogger('UberLoginViewModel');
  late String _applicationId;
  late WebViewController webviewContorller;
  late String _redirectUri;
  late String _url;

  late String _redirectCheck;
  late CubaLoginResponse _loginResponse;
  final _navigationService = locator<NavigationService>();
  final LoginModelServices _loginServices = locator<LoginModelServices>();
  final VehiclesModelServices _vehiclesModelServices =
      locator<VehiclesModelServices>();
  final IncidentListServices _incidentListServices =
      locator<IncidentListServices>();
  final _helper = locator<HelpModelServices>();
  final _dialogService = locator<DialogService>();

  String get redirectCheck => _redirectCheck;
  String get redirectUri => _redirectUri;
  String get initalUrl => _url;

  void initState() {
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
    //TODO change this into plugin callback
    _applicationId = 'com.cmtelematics.gemini.dashcam.dev';
    _url = Api.baseUrl + "?" + buildRedirectUri();
    log.d('initstae completed' + _url);
  }

  String buildRedirectUri() {
    String redirect = ".uberauth://redirect";
    _redirectCheck = _applicationId + redirect;
    _redirectUri = 'redirect_uri=' + _applicationId + redirect;
    return _redirectUri;
  }

  void navigateToDashboardView() {
    _navigationService.navigateTo(Routes.dashboardView);
  }

  void navigateToHomeView() {
    _navigationService.navigateTo(Routes.homeView);
  }

  void navigateToUberLoginView() {
    _navigationService.navigateTo(Routes.uberLoginView);
  }


  Future getCubeResponse({required NavigationRequest request}) async {
    setBusy(true);
    var authorizationCode;
    Uri.parse(request.url).queryParameters.forEach((key, value) {
      if (key == 'code') {
        authorizationCode = value;
      }
    });
    var result = await _loginServices.loginWithCode(
        authorizationCode: authorizationCode);
    //setBusy(false);
    notifyListeners();
    if (result is CubaLoginResponse) {
      log.d(" login successful");
      CubaLoginResponse login = _loginServices.loginResponse;
      authentication(request: result);
    } else {
      log.d(" login failed");
      await _dialogService.showCustomDialog(
        variant: DialogType.basic,
        title: ' Login ',
        description: ' Login Failed',
        mainButtonTitle: 'ok',
      );
      webviewContorller.clearCache();
      final cookieManager = CookieManager();
      cookieManager.clearCookies();
      navigateToUberLoginView();
    }
  }

  Future authentication({required CubaLoginResponse request}) async {
    setBusy(true);
    var loginrequest = request.toJson();
    var result =
        await _loginServices.sdkAuthentication(loginDetails: loginrequest);
    notifyListeners();

    if (result is bool) {
      if (result) {
        log.d(" login successful");
        var results = await _vehiclesModelServices.getVehicles();
        if (results is VehiclesModels) {
          log.d(" got vehicles $results");
          log.d(_vehiclesModelServices.VehicleResponse.vehicles.length);
          if (_vehiclesModelServices.VehicleResponse.vehicles.isNotEmpty) {
            var results = await _incidentListServices.getIncidentList();
            var re = await _helper.setHelperList();
            notifyListeners();
            if (results is IncidentListModel) {
              log.d(" received incident list");
              log.d(_incidentListServices
                  .IncidentListResponse.incidentList.length);
              navigateToHomeView();
            } else {
              log.d(" failed to get incident list +${results}");
              //show dialog
              await _dialogService.showCustomDialog(
                variant: DialogType.basic,
                title: ' VEHICLES ',
                description: ' EMPTY Vehicles',
                mainButtonTitle: 'ok',
              );
              webviewContorller.clearCache();
              final cookieManager = CookieManager();
              cookieManager.clearCookies();
              navigateToUberLoginView();
            }
          }
        } else {
          log.d(" getting vehicles failed");
        }
      } else {
        log.d("Login failed");
      }
    } else {
      log.d(" login failed");
    }
  }
}
