
import 'package:geminiapp/app/app.locator.dart';
import 'package:geminiapp/app/app.logger.dart';
import 'package:geminiapp/models/vehiclesmodel.dart';
import 'package:geminiapp/services/sdkservices.dart';
import 'package:injectable/injectable.dart';


@lazySingleton
class VehiclesModelServices {
  final log = getLogger('VehiclesModelServices');
  late VehiclesModels _vehicleResponse;
  VehiclesModels get VehicleResponse => _vehicleResponse;
  final _sdkService = locator<SDKServices>();

  Future<dynamic> getVehicles() async{
    try{
      var result  =  await _sdkService.sdkGetVehicles();
      _vehicleResponse = VehiclesModels.fromJson(result);
      return _vehicleResponse;
    }
    catch(e){
      log.d("Error Message :${e}");
      return "error Message"+e.toString();
    }

  }


}