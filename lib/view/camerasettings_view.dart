import 'package:flutter/material.dart';
import 'package:geminiapp/res/gradient.dart';
import 'package:geminiapp/view/sizeconfig.dart';
import 'package:geminiapp/view_model/camerasettings_viewmodel.dart';
import 'package:stacked/stacked.dart';
import '../res/customcolors.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class Camerasettings_View extends StatefulWidget {
  const Camerasettings_View({Key? key}) : super(key: key);
  static var isSwitched_road=true,isSwitched_cabin=true;

  @override
  _CamerasettingsViewState createState() => _CamerasettingsViewState();
}

class _CamerasettingsViewState extends State<Camerasettings_View> {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<CamerasettingsViewModel>.reactive(
        viewModelBuilder: () => CamerasettingsViewModel(),
        builder: (context, viewModel, child) {
          return Scaffold(
            body: Stack(
                //clipBehavior: Clip.none,
                children: [
                  Container(
                      decoration: BoxDecoration(
                          gradient: CustomGradient.linearGradient),
                      height: MediaQuery.of(context).size.height),
                  Positioned(
                    top: SizeConfig.screenheight*0.09,
                    left: 0,
                    right: 0,

                    child: Padding(
                        padding:
                        EdgeInsets.symmetric(  horizontal:SizeConfig.screenwidth*0.03),
                        child: Row(children: [
                          IconButton(
                            onPressed: () => Navigator.pop(context),
                            icon: const Icon(Icons.arrow_back_ios_new),
                            color: Colors.white,
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          Text(
                            AppLocalizations.of(context)!.cameraSettings,
                            style:const TextStyle(
                              decoration: TextDecoration.none,
                              fontSize: 25,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ])),
                  ),
                  Positioned(
                      top: SizeConfig.screenheight*0.18,
                      left: 0,
                      right: 0,
                      child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                          decoration: const BoxDecoration(
                              color: CustomColors.colorBackground,
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(30),
                                topRight: Radius.circular(30),
                              )),
                          child: Column(children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: ListTile(
                                title: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                     Padding(
                                      padding: EdgeInsets.all(8.0),
                                      child: Text(
                                          AppLocalizations.of(context)!.roadCamSetting,
                                        style: TextStyle(color: Colors.black,fontSize: SizeConfig.screenwidth*0.04),
                                      ),
                                    ),
                                    TextButton(
                                      onPressed: () => showDialog(
                                        context: context,
                                        builder: (BuildContext context) =>
                                            AlertDialog(
                                          title: Text(AppLocalizations.of(context)!.roadCamAlertTitle),
                                          content:Text(
                                            AppLocalizations.of(context)!.roadCamAlertMsg,
                                          ),
                                          actions: <Widget>[
                                            TextButton(
                                              onPressed: () =>
                                                  Navigator.pop(context, AppLocalizations.of(context)!.ok),
                                              child: Text(AppLocalizations.of(context)!.ok),
                                            ),
                                          ],
                                        ),
                                      ),
                                      child: Text(
                                        AppLocalizations.of(context)!.learnMore,
                                        style: TextStyle(
                                            color: Colors.blue, fontSize: 15),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ],
                                ),
                                //subtitle: ,
                                trailing: Switch(
                                  activeTrackColor: Colors.green,
                                  activeColor: Colors.white,
                                  value: Camerasettings_View.isSwitched_road,
                                  onChanged: (value) {
                                    setState(() {
                                      if (value == false) {
                                        Camerasettings_View.isSwitched_cabin =
                                            false;
                                        Camerasettings_View.isSwitched_road =
                                            false;
                                      } else {
                                        Camerasettings_View.isSwitched_road =
                                            value;
                                      }
                                    });
                                    viewModel.updateSettings("");
                                    if (viewModel.is_valid_vehicle_id == false) {
                                      final snackBar = SnackBar(
                                        content: const Text(
                                            'Invalid Short vehicle id'),
                                      );
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(snackBar);
                                    }
                                  },
                                ),
                              ),
                            ),
                            const Divider(
                              height: 0.5,
                              thickness: 0.5,
                              indent: 20,
                              endIndent: 20,
                              color: Colors.black26,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: ListTile(
                                title: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        "Keep cabin camera on during personal time",
                                        style: TextStyle(
                                            color: !Camerasettings_View.isSwitched_road
                                                ? Colors.black45
                                                : Colors.black),
                                      ),
                                    ),
                                    TextButton(
                                      onPressed: () => showDialog(
                                        context: context,
                                        builder: (BuildContext context) =>
                                            AlertDialog(
                                          title: const Text("Cabin footage"),
                                          content: const Text(
                                              "Cabin-facing footage will be captured. Cabin recording will contain audio, and can be very useful in case you get rear ended."),
                                          actions: <Widget>[
                                            TextButton(
                                              onPressed: () =>
                                                  Navigator.pop(context, 'OK'),
                                              child: const Text('OK'),
                                            ),
                                          ],
                                        ),
                                      ),
                                      child: const Text(
                                        'Learn more',
                                        style: TextStyle(
                                            color: Colors.blue, fontSize: 15),
                                        textAlign: TextAlign.left,
                                      ),
                                    ),
                                  ],
                                ),
                                trailing: Switch(
                                  activeTrackColor: Colors.green,
                                  activeColor: Colors.white,
                                  value:Camerasettings_View.isSwitched_cabin,
                                  onChanged: (value) {
                                    setState(() {
                                      if (Camerasettings_View.isSwitched_road == false) {
                                        Camerasettings_View.isSwitched_cabin = false;
                                      } else {
                                        Camerasettings_View.isSwitched_cabin = value;
                                      }
                                    });
                                  },
                                ),
                              ),
                            ),
                            const Divider(
                              height: 0.5,
                              thickness: 0.5,
                              indent: 20,
                              endIndent: 20,
                              color: Colors.black26,
                            ),
                          ])))
                ]),
          );
        });
  }
}
