import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:geminiapp/models/incidentmodel.dart';

class IncidentListModel {
  List<IncidentModel> incidentList;
  IncidentListModel({
    required this.incidentList,
  });

  IncidentListModel copyWith({
    List<IncidentModel>? incidentList,
  }) {
    return IncidentListModel(
      incidentList: incidentList ?? this.incidentList,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'results': incidentList.map((x) => x.toMap()).toList(),
    };
  }

  factory IncidentListModel.fromMap(Map<String, dynamic> map) {
    return IncidentListModel(
      incidentList: List<IncidentModel>.from(
          map['results']?.map((x) => IncidentModel.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory IncidentListModel.fromJson(String source) =>
      IncidentListModel.fromMap(json.decode(source));

  @override
  String toString() => 'IncidentList(incidentList: $incidentList)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is IncidentListModel &&
        listEquals(other.incidentList, incidentList);
  }

  @override
  int get hashCode => incidentList.hashCode;
}
